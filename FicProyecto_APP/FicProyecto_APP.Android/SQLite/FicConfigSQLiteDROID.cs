﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using FicProyecto_APP.Droid.SQLite;
using FicProyecto_APP.Interfaces.SQLite;
using Xamarin.Forms;


[assembly: Dependency(typeof(FicConfigSQLiteDROID))]
namespace FicProyecto_APP.Droid.SQLite
{
    class FicConfigSQLiteDROID : IFicConfigSQLite
    {
        public string FicGetDataBasePath()
        {
           

            var FicPathFile = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads);
            //var FicDirectorioDB = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var FicDirectorioDB = FicPathFile.Path;
            FicDirectorioDB = FicDirectorioDB + "/Carreras/";
            string FicPathDB = Path.Combine(FicDirectorioDB, FicAppSettings.FicDataBaseName);
            try
            {
                Directory.CreateDirectory(FicDirectorioDB);
            }
            catch (Exception ex)
            {
            }
            return FicPathDB;
        }

    }
}