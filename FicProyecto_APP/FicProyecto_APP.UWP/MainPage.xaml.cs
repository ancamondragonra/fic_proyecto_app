﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace FicProyecto_APP.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();

            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("Mzg5NjJAMzEzNjJlMzMyZTMwajVhNlhuUVU2MEZmNHJJZWFTSVRhYU5hdHE1b2s1QzdNTjlJbW90UkE1UT0=");
            Syncfusion.SfDataGrid.XForms.UWP.SfDataGridRenderer.Init();
            LoadApplication(new FicProyecto_APP.App());
        }
    }
}
