﻿using FicProyecto_APP.ViewModels.Base;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace FicProyecto_APP
{
    public partial class App : Application
    {
        private static FicViewModelLocator ficVmLocator;
        public static FicViewModelLocator FicMetLocator
        {
            get { return ficVmLocator = ficVmLocator ?? new FicViewModelLocator(); }
        }

        public static object FicVmLocator { get; internal set; }


        public App()
        {
            InitializeComponent();
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("Mzg5NjJAMzEzNjJlMzMyZTMwajVhNlhuUVU2MEZmNHJJZWFTSVRhYU5hdHE1b2s1QzdNTjlJbW90UkE1UT0=");
            MainPage = new Views.Navegacion.FicMasterPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
