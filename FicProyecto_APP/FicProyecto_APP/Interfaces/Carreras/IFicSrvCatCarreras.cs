﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static FicProyecto_APP.Models.FicCarrera;

namespace FicProyecto_APP.Interfaces.Carreras
{
    public interface  IFicSrvCatCarreras
    {
        Task<IList<eva_cat_carreras>> FicMetGetListCarreras();  
        Task FicMetRemoveCarrera(eva_cat_carreras eva_cat_carreras);    // DELETE ITEM
        Task<eva_cat_carreras> FicMetGetMaxCarrerasId();
        Task FicMetInsertCarrera(eva_cat_carreras eva_cat_carreras);
        Task FicMetUpdateCarrera(eva_cat_carreras eva_cat_carreras);
        Task<IList<cat_generales>> GetGeneralesByTipo(Int16 id);
    }
}
