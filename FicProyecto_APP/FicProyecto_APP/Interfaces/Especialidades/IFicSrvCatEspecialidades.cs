﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static FicProyecto_APP.Models.FicCarrera;

namespace FicProyecto_APP.Interfaces.Especialidades
{
    public interface IFicSrvCatEspecialidades
    {
        /**************ESPECIALIDADES**************/
        Task<IList<eva_cat_especialidades>> FicMetGetListEspecialidades();
        Task FicMetInsertCatEspecialidad(eva_cat_especialidades data);
        Task FicMetUpdateCatEspecialidad(eva_cat_especialidades data);
        Task<eva_cat_especialidades> FicMetGetMaxCatEspecialiddadIdAsync();
        Task FicMetRemoveCat_Especialidades(eva_cat_especialidades data);

        
        Task<IList<eva_cat_especialidades>> FicMetGetListEspecialidadesByCarrera(Int16 data);
        Task<eva_carreras_especialidades> FicMetGetListEspecialidadesByCarrera2(Int16 data, Int16 data2);


        /*********CARREAS ESPECIALIDADES**********/
        Task<IList<eva_carreras_especialidades>> FicGetCarrerasEspecialidades();
        Task FicMetInsertCarreraEspecialidad(eva_carreras_especialidades data);
        Task FicMetUpdateCarreraEspecialidad(eva_carreras_especialidades data);
        Task<IList<eva_cat_carreras>> FicMetGetListCarreras();
        Task<eva_carreras_especialidades> FicMetGetMaxCarrerasEspecialiddadIdAsync();
        Task FicMetRemoveCarrera_Especialidades(eva_carreras_especialidades data);
        
    }
}
