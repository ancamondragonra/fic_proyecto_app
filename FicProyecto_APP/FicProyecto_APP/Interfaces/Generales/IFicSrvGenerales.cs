﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static FicProyecto_APP.Models.FicCarrera;

namespace FicProyecto_APP.Interfaces.Generales
{
    public interface IFicSrvGenerales
    {
        Task<IList<cat_generales>> FicGetGenerales();
        Task<IList<cat_tipos_generales>> FicGetGeneralesTipo();
    }
}
