﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace FicProyecto_APP.Interfaces.ImportarExportar
{
    public interface IFicSrvExportarWebApi
    {
        Task<string> FicPostExportCarreras();
    }

}
