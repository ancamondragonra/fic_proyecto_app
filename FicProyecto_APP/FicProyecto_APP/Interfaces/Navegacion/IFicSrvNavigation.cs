﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FicProyecto_APP.Navegacion
{
    public interface IFicSrvNavigation
    {
        void FicMetNavigateTo<FicTDestinationViewModel>(object FicNavigationContext = null);
        void FicMetNavigateTo(Type FicDestinationType, object FicNavigationContext = null);
        void FicMetNavigateTo<FicTDestinationViewModel>(object FicNavigationContext = null, bool show = true);
        void FicMetNavigateBack();
    }
}
