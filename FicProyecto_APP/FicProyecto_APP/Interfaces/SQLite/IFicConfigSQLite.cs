﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FicProyecto_APP.Interfaces.SQLite;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;



namespace FicProyecto_APP.Interfaces.SQLite
{
    public interface IFicConfigSQLite
    {
        //TRAE LA RUTA DE LA BASE
        string FicGetDataBasePath();
    }

}
