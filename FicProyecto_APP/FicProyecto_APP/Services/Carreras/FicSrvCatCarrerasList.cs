﻿using FicProyecto_APP.Data;
using FicProyecto_APP.Helpers;
using FicProyecto_APP.Interfaces.SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;
using System.Linq;
using FicProyecto_APP.Interfaces.Carreras;
using Microsoft.EntityFrameworkCore;

namespace FicProyecto_APP.Services.Carreras
{
    public class FicSrvCatCarrerasList : IFicSrvCatCarreras
    {
        private static readonly IFicAsyncLock ficMutex = new IFicAsyncLock();
        private FicDBContext FicLoBDContext;
        public FicSrvCatCarrerasList()
        {
            try{
                FicLoBDContext = new FicDBContext(DependencyService.Get<IFicConfigSQLite>().FicGetDataBasePath());
            }
            catch (Exception e)
            {

            }
           
        }

        #region eva_cat_carreras
        public async Task<IList<eva_cat_carreras>> FicMetGetListCarreras()
        {
            var items = new List<eva_cat_carreras>();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var resultado = from FicCED in FicLoBDContext.eva_cat_carreras
                                select new eva_cat_carreras
                                {
                                    IdCarrera = FicCED.IdCarrera,
                                    Alias = FicCED.Alias,
                                    DesCarrera = FicCED.DesCarrera,
                                    ClaveOficial = FicCED.ClaveOficial,
                                    ClaveCarrera = FicCED.ClaveCarrera,
                                    Creditos = FicCED.Creditos,
                                    NombreCorto = FicCED.NombreCorto,
                                    Activo = FicCED.Activo,
                                    Borrado = FicCED.Borrado,
                                    FechaFin = FicCED.FechaFin,
                                    FechaIni = FicCED.FechaIni,
                                    FechaReg = FicCED.FechaReg,
                                    FechaUltMod = FicCED.FechaUltMod,
                                    UsuarioMod = FicCED.UsuarioMod,
                                    UsuarioReg = FicCED.UsuarioReg,
                                    IdGenGradoEscolar = FicCED.IdGenGradoEscolar,
                                    IdTipoGenGradoEscolar = FicCED.IdTipoGenGradoEscolar,
                                    IdGenModalidad = FicCED.IdGenModalidad,
                                    IdTipoGenModalidad = FicCED.IdTipoGenModalidad,
                                    cat_generales = (
                                   from FG in FicLoBDContext.cat_generales
                                   where FG.IdGeneral == FicCED.IdGenGradoEscolar
                                   select new cat_generales
                                   {
                                       IdGeneral = FG.IdGeneral,
                                       DesGeneral = FG.DesGeneral,
                                   }).First(),
                                    cat_tipos_generales = (
                                   from FG2 in FicLoBDContext.cat_tipos_generales
                                   where FG2.IdTipoGeneral == FicCED.IdTipoGenGradoEscolar
                                   select new cat_tipos_generales
                                   {
                                       IdTipoGeneral = FG2.IdTipoGeneral,
                                       DesTipo = FG2.DesTipo,
                                   }).First(),
                                };
                resultado.ToList().ForEach(x => items.Add(x));
            }
            return items;
        }
        public async Task FicMetRemoveCarrera(eva_cat_carreras item)
        {
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                FicLoBDContext.Entry(item).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                FicLoBDContext.SaveChanges();
            }
        }

        public async Task<eva_cat_carreras> FicMetGetMaxCarrerasId()
        {
            var item = new eva_cat_carreras();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var r = FicLoBDContext.eva_cat_carreras.Max(x => x.IdCarrera);
                item.IdCarrera = r;
            }
            return item;
        }

        public async Task FicMetInsertCarrera(eva_cat_carreras item)
        {
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                FicLoBDContext.eva_cat_carreras.Add(item);
                FicLoBDContext.SaveChanges();
            }
        }

        public async Task FicMetUpdateCarrera(eva_cat_carreras item)
        {
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                FicLoBDContext.eva_cat_carreras.Update(item);
                FicLoBDContext.SaveChanges();
                FicLoBDContext.Entry<eva_cat_carreras>(item).State = EntityState.Detached;
            }
        }
        

        #endregion
        #region cat_generales
        public async Task<IList<cat_generales>> GetGeneralesByTipo(Int16 id)
        {
            var items = new List<cat_generales>();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var resultado = from FicCED in FicLoBDContext.cat_generales where FicCED.IdTipoGeneral == id
                                select new
                                { FicCED };
                resultado.ToList().ForEach(x => items.Add(x.FicCED));
            }
            return items;
        }

        #endregion
    }
}
