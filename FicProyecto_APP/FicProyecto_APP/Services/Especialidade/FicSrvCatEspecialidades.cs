﻿using FicProyecto_APP.Data;
using FicProyecto_APP.Helpers;
using FicProyecto_APP.Interfaces.Especialidades;
using FicProyecto_APP.Interfaces.SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using FicProyecto_APP.Models;

namespace FicProyecto_APP.Services.Especialidade
{
    public class FicSrvCatEspecialidades : IFicSrvCatEspecialidades
    {
        private static readonly IFicAsyncLock ficMutex = new IFicAsyncLock();
        private FicDBContext FicLoBDContext;
        public FicSrvCatEspecialidades()
        {
            FicLoBDContext = new FicDBContext(DependencyService.Get<IFicConfigSQLite>().FicGetDataBasePath());
        }
        /*--------------------------------------- ESPECIALIDAD------------------------------------------------*/
        #region eva_cat_especialidades
        public async Task<IList<eva_cat_especialidades>> FicMetGetListEspecialidades()
        {
            var items = new List<eva_cat_especialidades>();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var resultado = from FicCED in FicLoBDContext.eva_cat_especialidades
                                select new 
                                { FicCED };
                resultado.ToList().ForEach(x => items.Add(x.FicCED));

            }
            return items;
        }
        #endregion


        #region eva_cat_especialidades
        public async Task<IList<eva_cat_especialidades>> FicMetGetListEspecialidadesByCarrera(Int16 id)
        {
            var items = new List<eva_cat_especialidades>();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var resultado = from FicCED in FicLoBDContext.eva_cat_especialidades  
                                from FicCDD in FicLoBDContext.eva_carreras_especialidades
                                where FicCED.IdEspecialidad== FicCDD.IdEspecialidad  && FicCDD.IdCarrera==id
                                select new  
                                { FicCED };
                resultado.ToList().ForEach(x => items.Add(x.FicCED));

            }
            return items;
        }
        #endregion

        #region eva_cat_especialidades
        public async Task<eva_carreras_especialidades>FicMetGetListEspecialidadesByCarrera2(Int16 id, Int16 id2)
        {
            var items = new eva_carreras_especialidades();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var resultado = from FicCDD in FicLoBDContext.eva_carreras_especialidades
                                where id2 == FicCDD.IdEspecialidad && FicCDD.IdCarrera == id
                                select new eva_carreras_especialidades
                                {
                                    IdCarreraEspecilidad= FicCDD.IdCarreraEspecilidad
                                };
                items = resultado.First();
            }
            return items;
        }
        #endregion


        #region eva_carreras_especialidades
        public async Task<IList<eva_carreras_especialidades>> FicGetCarrerasEspecialidades()
        {
            var items = new List<eva_carreras_especialidades>();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var resultado = from FicCED in FicLoBDContext.eva_carreras_especialidades
                                select new eva_carreras_especialidades
                                {
                                    IdCarrera = FicCED.IdCarrera,
                                    IdCarreraEspecilidad = FicCED.IdCarreraEspecilidad,
                                    IdEspecialidad = FicCED.IdEspecialidad,
                                    Activo = FicCED.Activo,
                                    Borrado = FicCED.Borrado,
                                    FechaFin = FicCED.FechaFin,
                                    FechaIni = FicCED.FechaIni,
                                    FechaReg = FicCED.FechaReg,
                                    FechaUltMod = FicCED.FechaUltMod,
                                    UsuarioMod = FicCED.UsuarioMod,
                                    UsuarioReg = FicCED.UsuarioReg,
                                    eva_cat_carreras = (
                                    from FG in FicLoBDContext.eva_cat_carreras
                                    where FG.IdCarrera == FicCED.IdCarrera
                                    select new eva_cat_carreras
                                    {
                                       DesCarrera = FG.DesCarrera,
                                       ClaveCarrera = FG.ClaveCarrera,
                                    }).First(),
                                    eva_cat_especialidades = (
                                    from FG in FicLoBDContext.eva_cat_especialidades
                                    where FG.IdEspecialidad == FicCED.IdEspecialidad
                                    select new eva_cat_especialidades
                                    {
                                        DesEspecialidad = FG.DesEspecialidad,
                                    }).First(),
                                };
                resultado.ToList().ForEach(x => items.Add(x));

            }
            return items;
        }
        #endregion


        public async Task<eva_cat_especialidades> FicMetGetMaxCatEspecialiddadIdAsync()
        {
            var item = new eva_cat_especialidades();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var r = FicLoBDContext.eva_cat_especialidades.Max(x => x.IdEspecialidad);
                item.IdEspecialidad = r;
            }
            return item;
        }
        public async Task FicMetInsertCatEspecialidad(eva_cat_especialidades item)
        {
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                FicLoBDContext.eva_cat_especialidades.Add(item);
                FicLoBDContext.SaveChanges();
                FicLoBDContext.Entry<eva_cat_especialidades>(item).State = EntityState.Detached;
            }
        }
        public async Task FicMetUpdateCatEspecialidad(eva_cat_especialidades item)
        {
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                FicLoBDContext.eva_cat_especialidades.Update(item);
                FicLoBDContext.SaveChanges();
                FicLoBDContext.Entry<eva_cat_especialidades>(item).State = EntityState.Detached;
            }
        }
        public async Task FicMetRemoveCat_Especialidades(eva_cat_especialidades item)
        {
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                FicLoBDContext.Entry(item).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                FicLoBDContext.SaveChanges();

            }
        } 

        /*-------------------------------CARRERA ESPECIALIDAD------------------------------------------*/

        public async Task FicMetInsertCarreraEspecialidad(eva_carreras_especialidades item)
        {
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                FicLoBDContext.eva_carreras_especialidades.Add(item);
                FicLoBDContext.SaveChanges();
                FicLoBDContext.Entry<eva_carreras_especialidades>(item).State = EntityState.Detached;
            }
        }
        public async Task FicMetUpdateCarreraEspecialidad(eva_carreras_especialidades item)
        {
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                FicLoBDContext.eva_carreras_especialidades.Update(item);
                FicLoBDContext.SaveChanges();
                FicLoBDContext.Entry<eva_carreras_especialidades>(item).State = EntityState.Detached;
            }
        }
        public async Task<IList<eva_cat_carreras>> FicMetGetListCarreras()
        {
            var items = new List<eva_cat_carreras>();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var resultado = from FicCED in FicLoBDContext.eva_cat_carreras
                                select new eva_cat_carreras
                                {
                                    IdCarrera = FicCED.IdCarrera,
                                    Alias = FicCED.Alias,
                                    DesCarrera = FicCED.DesCarrera,
                                    ClaveOficial = FicCED.ClaveOficial,
                                    ClaveCarrera = FicCED.ClaveCarrera,
                                    Creditos = FicCED.Creditos,
                                    NombreCorto = FicCED.NombreCorto,
                                    Activo = FicCED.Activo,
                                    Borrado = FicCED.Borrado,
                                    FechaFin = FicCED.FechaFin,
                                    FechaIni = FicCED.FechaIni,
                                    FechaReg = FicCED.FechaReg,
                                    FechaUltMod = FicCED.FechaUltMod,
                                    UsuarioMod = FicCED.UsuarioMod,
                                    UsuarioReg = FicCED.UsuarioReg,
                                    IdGenGradoEscolar = FicCED.IdGenGradoEscolar,
                                    IdTipoGenGradoEscolar = FicCED.IdTipoGenGradoEscolar,
                                    IdGenModalidad = FicCED.IdGenModalidad,
                                    IdTipoGenModalidad = FicCED.IdTipoGenModalidad,
                                    cat_generales = (
                                   from FG in FicLoBDContext.cat_generales
                                   where FG.IdGeneral == FicCED.IdGenGradoEscolar
                                   select new cat_generales
                                   {
                                       IdGeneral = FG.IdGeneral,
                                       DesGeneral = FG.DesGeneral,
                                   }).First(),
                                    cat_tipos_generales = (
                                   from FG2 in FicLoBDContext.cat_tipos_generales
                                   where FG2.IdTipoGeneral == FicCED.IdTipoGenGradoEscolar
                                   select new cat_tipos_generales
                                   {
                                       IdTipoGeneral = FG2.IdTipoGeneral,
                                       DesTipo = FG2.DesTipo,
                                   }).First(),
                                };
                resultado.ToList().ForEach(x => items.Add(x));
            }
            return items;
        }

        public async Task<eva_carreras_especialidades> FicMetGetMaxCarrerasEspecialiddadIdAsync()
        {
            var item = new eva_carreras_especialidades();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var r = FicLoBDContext.eva_carreras_especialidades.Max(x => x.IdCarreraEspecilidad);
                item.IdCarreraEspecilidad = r;
            }
            return item;
        }

        public async Task FicMetRemoveCarrera_Especialidades(eva_carreras_especialidades item)
        {
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                FicLoBDContext.Entry(item).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                FicLoBDContext.SaveChanges();
            }
        }
    }
}
