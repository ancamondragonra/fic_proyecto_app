﻿using FicProyecto_APP.Data;
using FicProyecto_APP.Helpers;
using FicProyecto_APP.Interfaces.Generales;
using FicProyecto_APP.Interfaces.SQLite;
using FicProyecto_APP.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;
using System.Linq;

using Microsoft.EntityFrameworkCore;

namespace FicProyecto_APP.Services.Generales
{
    public class FicSrvCatGenerales : IFicSrvGenerales
    {
        private static readonly IFicAsyncLock ficMutex = new IFicAsyncLock();
        private FicDBContext FicLoBDContext;
        public FicSrvCatGenerales()
        {
            FicLoBDContext = new FicDBContext(DependencyService.Get<IFicConfigSQLite>().FicGetDataBasePath());
        }

        public async Task<IList<cat_generales>> FicGetGenerales()
        {
            var items = new List<cat_generales>();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var resultado = from FicCED in FicLoBDContext.cat_generales
                                select new
                                { FicCED };
                resultado.ToList().ForEach(x => items.Add(x.FicCED));
            }
            return items;
        }

        public async Task<IList<cat_tipos_generales>> FicGetGeneralesTipo()
        {
            var items = new List<cat_tipos_generales>();
            using (await ficMutex.LockAsync().ConfigureAwait(false))
            {
                var resultado = from FicCED in FicLoBDContext.cat_tipos_generales
                                select new
                                { FicCED };
                resultado.ToList().ForEach(x => items.Add(x.FicCED));
            }
            return items;
        }
    }
}
