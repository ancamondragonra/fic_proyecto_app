﻿using FicProyecto_APP.Data;
using FicProyecto_APP.Interfaces.ImportarExportar;
using FicProyecto_APP.Interfaces.SQLite;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;
using Microsoft.EntityFrameworkCore;
using System.Linq;


namespace FicProyecto_APP.Services.ImportarExportar
{
    public class FicSrvExportarWebApi : IFicSrvExportarWebApi
    {
        private readonly FicDBContext FicLoBDContext;
        private readonly HttpClient FiClient;
        private string base_url = "http://localhost:54868/";
        public FicSrvExportarWebApi()
        {
            FicLoBDContext = new FicDBContext(DependencyService.Get<IFicConfigSQLite>().FicGetDataBasePath());
            FiClient = new HttpClient();
            FiClient.MaxResponseContentBufferSize = 256000;

        }//CONSTRUCTOR

        private async Task<string> FicPostListCarreras(cat_exportar_importar item)
        {
            string url = base_url + "api/exportar/setall";
            HttpResponseMessage response = await FiClient.PostAsync(
                new Uri(string.Format(url, string.Empty)),
                new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8, "application/json")
            );
            return await response.Content.ReadAsStringAsync();
        }//POST: A INVENTARIOS

        public async Task<string> FicPostExportCarreras()
        {
            return await FicPostListCarreras(new cat_exportar_importar()
            {
                eva_cat_especialidades = await (from a in FicLoBDContext.eva_cat_especialidades select a).AsNoTracking().ToListAsync(),
                eva_cat_carreras = await (from a in FicLoBDContext.eva_cat_carreras select a).AsNoTracking().ToListAsync(),
                eva_carreras_especialidades = await (from a in FicLoBDContext.eva_carreras_especialidades select a).AsNoTracking().ToListAsync(),
                cat_tipos_generales = await (from a in FicLoBDContext.cat_tipos_generales select a).AsNoTracking().ToListAsync(),
                cat_generales = await (from a in FicLoBDContext.cat_generales select a).AsNoTracking().ToListAsync(),
                eva_carreras_reticulas = await (from a in FicLoBDContext.eva_carreras_reticulas select a).AsNoTracking().ToListAsync(),
                eva_cat_reticulas = await (from a in FicLoBDContext.eva_cat_reticulas select a).AsNoTracking().ToListAsync(),
            });

        }//METODO DE EXPORT INVENTARIOS
        
    }

}
