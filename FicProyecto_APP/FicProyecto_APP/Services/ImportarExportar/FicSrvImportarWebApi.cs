﻿using FicProyecto_APP.Data;
using FicProyecto_APP.Interfaces.ImportarExportar;
using FicProyecto_APP.Interfaces.SQLite;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace FicProyecto_APP.Services.ImportarExportar
{
    class FicSrvImportarWebApi : IFicSrvImportarWebApi
    {
        private readonly FicDBContext FicLoBDContext;
        private readonly HttpClient FiClient;
        private string base_url = "http://localhost:54868/";
        public FicSrvImportarWebApi()
        {
            FicLoBDContext = new FicDBContext(DependencyService.Get<IFicConfigSQLite>().FicGetDataBasePath());
            FiClient = new HttpClient();
            FiClient.MaxResponseContentBufferSize = 256000;
        }//CONSTRUCTOR

        /*private async Task<cat_exportar_importar> FicGetListCarreras2(Int16 id = 0)
        {
            try
            {
                string url = "";
                if (id != 0) url = base_url + "api/edificio/get" + "?idEdificio=" + id;
                else url = base_url + "api/edificio/all";
                var response = await FiClient.GetAsync(new Uri(url));
                return response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<cat_exportar_importar>(await response.Content.ReadAsStringAsync()) : null;
            }
            catch (Exception e)
            {
                await new Page().DisplayAlert("ALERTA", e.Message.ToString(), "OK");
                return null;
            }
        }
        */
        private async Task<cat_exportar_importar> FicGetListCarreras1()
        {
            string url = base_url + "api/importar/getall";

            try
            {
                var response = await FiClient.GetAsync(url);
                return response.IsSuccessStatusCode ? JsonConvert.DeserializeObject<cat_exportar_importar>(await response.Content.ReadAsStringAsync()) : null;
            }
            catch (Exception e)
            {
                await new Page().DisplayAlert("ALERTA", e.Message.ToString(), "OK");
                return null;
            }
        }

        /*------------------ VALIDADORES --------------------*/
        private async Task<cat_generales> FicExistzt_cat_generales(Int16 id)
        {
            return await (
                from FicCED in FicLoBDContext.cat_generales
                where FicCED.IdGeneral == id
                select FicCED)
                .AsNoTracking()
                .SingleOrDefaultAsync();
        }

        private async Task<cat_tipos_generales> FicExistzt_cat_tipo_generales(Int16 id)
        {
            return await (
                from FicCED in FicLoBDContext.cat_tipos_generales
                where FicCED.IdTipoGeneral == id
                select FicCED)
                .AsNoTracking()
                .SingleOrDefaultAsync();
        }
        private async Task<eva_cat_carreras> FicExistzt_eva_cat_carreras(Int16 id)
        {
            return await (
                from FicCED in FicLoBDContext.eva_cat_carreras
                where FicCED.IdCarrera == id
                select FicCED)
                .AsNoTracking()
                .SingleOrDefaultAsync();
        }

        private async Task<eva_cat_especialidades> FicExistzt_eva_cat_especialidades(Int16 id)
        {
            return await (
                from FicCED in FicLoBDContext.eva_cat_especialidades
                where FicCED.IdEspecialidad == id
                select FicCED)
                .AsNoTracking()
                .SingleOrDefaultAsync();
        }
        private async Task<eva_carreras_especialidades> FicExistzt_eva_carreras_especialidades(Int16 id)
        {
            return await (
                from FicCED in FicLoBDContext.eva_carreras_especialidades
                where FicCED.IdCarreraEspecilidad == id
                select FicCED)
                .AsNoTracking()
                .SingleOrDefaultAsync();
        }

        private async Task<eva_cat_reticulas> FicExistzt_eva_cat_reticulas(Int16 id)
        {
            return await (
                from FicCED in FicLoBDContext.eva_cat_reticulas
                where FicCED.IdReticula == id
                select FicCED)
                .AsNoTracking()
                .SingleOrDefaultAsync();
        }

        private async Task<eva_carreras_reticulas> FicExistzt_eva_carreras_reticulas(Int16 id)
        {
            return await (
                from FicCED in FicLoBDContext.eva_carreras_reticulas
                where FicCED.IdCarreraRiticula == id
                select FicCED)
                .AsNoTracking()
                .SingleOrDefaultAsync();
        }

        public async Task<string> FicGetCarreras()
        {
            string FicMensaje = "";
            try
            {
                FicMensaje = "IMPORTACION: \n";
                var FicGetReultREST = await FicGetListCarreras1();

          
                if (FicGetReultREST != null && FicGetReultREST.cat_tipos_generales != null)
                {
                    FicMensaje += "IMPORTANDO: cat_tipos_generales \n";
                    foreach (cat_tipos_generales inv in FicGetReultREST.cat_tipos_generales)
                    {
                        var respuesta = await FicExistzt_cat_tipo_generales(inv.IdTipoGeneral);
                        if (respuesta != null)
                        {
                            try
                            {
                                respuesta.IdTipoGeneral = inv.IdTipoGeneral;
                                respuesta.DesTipo = inv.DesTipo;
                                respuesta.FechaUltMod = inv.FechaUltMod;
                                respuesta.FechaReg = inv.FechaReg;
                                respuesta.FechaUltMod = inv.FechaUltMod;
                                respuesta.Borrado = inv.Borrado;
                                respuesta.FechaReg = inv.FechaReg;
                                respuesta.UsuarioMod = inv.UsuarioMod;
                                respuesta.UsuarioReg = inv.UsuarioReg;
                                FicLoBDContext.Update(respuesta);

                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-UPDATE-> IdTipoGeneral: " + inv.IdTipoGeneral + " \n" : "-NO NECESITO ACTUALIZAR-> IdTipoGeneral: " + inv.IdTipoGeneral + " \n";
                                FicLoBDContext.Entry<cat_tipos_generales>(respuesta).State = EntityState.Detached;
                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                        else
                        {
                            try
                            {
                                FicLoBDContext.Add(inv);
                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-INSERT-> IdTipoGeneral: " + inv.IdTipoGeneral + " \n" : "-ERROR EN INSERTAR-> IdTipoGeneral: " + inv.IdTipoGeneral + " \n";
                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                    }
                }
                else FicMensaje += "-> SIN DATOS. \n";
                if (FicGetReultREST != null && FicGetReultREST.cat_generales != null)
                {
                    FicMensaje += "IMPORTANDO: cat_generales \n";
                    foreach (cat_generales inv in FicGetReultREST.cat_generales)
                    {
                        var respuesta = await FicExistzt_cat_generales(inv.IdGeneral);
                        if (respuesta != null)
                        {
                            try
                            {
                                respuesta.IdGeneral = inv.IdGeneral;
                                respuesta.IdLlaveClasifica = inv.IdLlaveClasifica;
                                respuesta.FechaUltMod = inv.FechaUltMod;
                                respuesta.DesGeneral = inv.DesGeneral;
                                respuesta.Activo = inv.Activo;
                                respuesta.Borrado = inv.Borrado;
                                respuesta.Clave = inv.Clave;
                                respuesta.FechaReg = inv.FechaReg;
                                respuesta.IdTipoGeneral = inv.IdTipoGeneral;
                                respuesta.UsuarioMod = inv.UsuarioMod;
                                respuesta.UsuarioReg = inv.UsuarioReg;
                                FicLoBDContext.Update(respuesta);

                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-UPDATE-> IdGeneral: " + inv.IdGeneral + " \n" : "-NO NECESITO ACTUALIZAR-> IdGeneral: " + inv.IdGeneral + " \n";
                                FicLoBDContext.Entry<cat_generales>(respuesta).State = EntityState.Detached;
                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                        else
                        {
                            try
                            {
                                FicLoBDContext.Add(inv);
                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-INSERT-> IdGeneral: " + inv.IdGeneral + " \n" : "-ERROR EN INSERTAR-> IdGeneral: " + inv.IdGeneral + " \n";
                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                    }
                }
                else FicMensaje += "-> SIN DATOS. \n";
                if (FicGetReultREST != null && FicGetReultREST.eva_cat_carreras != null)
                {
                    FicMensaje += "IMPORTANDO: eva_cat_carreras \n";
                    foreach (eva_cat_carreras inv in FicGetReultREST.eva_cat_carreras)
                    {
                        var respuesta = await FicExistzt_eva_cat_carreras(inv.IdCarrera);
                        if (respuesta != null)
                        {
                            try
                            {
                                respuesta.IdCarrera = inv.IdCarrera;
                                respuesta.ClaveCarrera = inv.ClaveCarrera;
                                respuesta.ClaveOficial = inv.ClaveOficial;
                                respuesta.DesCarrera = inv.DesCarrera;
                                respuesta.IdGenGradoEscolar = inv.IdGenGradoEscolar;
                                respuesta.IdTipoGenGradoEscolar = inv.IdTipoGenGradoEscolar;
                                respuesta.IdGenModalidad = inv.IdGenModalidad;
                                respuesta.IdTipoGenModalidad = inv.IdTipoGenModalidad;
                                respuesta.FechaUltMod = inv.FechaUltMod;
                                respuesta.Activo = inv.Activo;
                                respuesta.Borrado = inv.Borrado;
                                respuesta.FechaReg = inv.FechaReg;
                                respuesta.UsuarioMod = inv.UsuarioMod;
                                respuesta.UsuarioReg = inv.UsuarioReg;
                                respuesta.NombreCorto = inv.NombreCorto;
                                respuesta.Creditos = inv.Creditos;
                                respuesta.FechaIni = inv.FechaIni;
                                respuesta.FechaFin = inv.FechaFin;
                                respuesta.cat_generales = null;
                                respuesta.cat_generales_modalidad = null;
                                respuesta.cat_tipos_generales = null;
                                respuesta.cat_tipos_generales_modalidad = null;
                                FicLoBDContext.Update(respuesta);
                                
                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-UPDATE-> IdCarrera: " + inv.IdCarrera + " \n" : "-NO NECESITO ACTUALIZAR-> IdCarrera: " + inv.IdCarrera + " \n";
                                FicLoBDContext.Entry<eva_cat_carreras>(respuesta).State = EntityState.Detached;
                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                        else
                        {
                            try
                            {
                                inv.cat_generales = null;
                                inv.cat_generales_modalidad = null;
                                inv.cat_tipos_generales = null;
                                inv.cat_tipos_generales_modalidad = null;
                                FicLoBDContext.Add(inv);
                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-INSERT-> IdCarrera: " + inv.IdCarrera + " \n" : "-ERROR EN INSERTAR-> IdGeneral: " + inv.IdCarrera + " \n";
                                FicLoBDContext.Entry<eva_cat_carreras>(respuesta).State = EntityState.Detached;
                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                    }
                }
                else FicMensaje += "-> SIN DATOS. \n";
                if (FicGetReultREST != null && FicGetReultREST.eva_cat_especialidades != null)
                {
                    FicMensaje += "IMPORTANDO: eva_cat_especialidades \n";
                    foreach (eva_cat_especialidades inv in FicGetReultREST.eva_cat_especialidades)
                    {
                        var respuesta = await FicExistzt_eva_cat_especialidades(inv.IdEspecialidad);
                        if (respuesta != null)
                        {
                            try
                            {
                                respuesta.IdEspecialidad = inv.IdEspecialidad;
                                respuesta.DesEspecialidad = inv.DesEspecialidad;
                                respuesta.Activo = inv.Activo;
                                respuesta.Borrado = inv.Borrado;
                                respuesta.FechaReg = inv.FechaReg;
                                respuesta.FechaUltMod = inv.FechaUltMod;
                                respuesta.UsuarioMod = inv.UsuarioMod;
                                respuesta.UsuarioReg = inv.UsuarioReg;

                                FicLoBDContext.Update(respuesta);

                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-UPDATE-> IdEspecialidad: " + inv.IdEspecialidad + " \n" : "-NO NECESITO ACTUALIZAR-> IdEspecialidad: " + inv.IdEspecialidad + " \n";
                                FicLoBDContext.Entry<eva_cat_especialidades>(respuesta).State = EntityState.Detached;
                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                        else
                        {
                            try
                            {
                                FicLoBDContext.Add(inv);
                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-INSERT-> IdEspecialidad: " + inv.IdEspecialidad + " \n" : "-ERROR EN INSERTAR-> IdEspecialidad: " + inv.IdEspecialidad + " \n";
                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                    }
                }
                else FicMensaje += "-> SIN DATOS. \n";
                if (FicGetReultREST != null && FicGetReultREST.eva_carreras_especialidades != null)
                {
                    FicMensaje += "IMPORTANDO: eva_carreras_especialidades \n";
                    foreach (eva_carreras_especialidades inv in FicGetReultREST.eva_carreras_especialidades)
                    {
                        var respuesta = await FicExistzt_eva_carreras_especialidades(inv.IdCarreraEspecilidad);
                        if (respuesta != null)
                        {
                            try
                            {
                                respuesta.IdCarreraEspecilidad = inv.IdCarreraEspecilidad;
                                respuesta.IdEspecialidad = inv.IdEspecialidad;
                                respuesta.IdCarrera = inv.IdCarrera;
                                respuesta.Activo = inv.Activo;
                                respuesta.Borrado = inv.Borrado;
                                respuesta.FechaReg = inv.FechaReg;
                                respuesta.FechaUltMod = inv.FechaUltMod;
                                respuesta.FechaIni = inv.FechaIni;
                                respuesta.FechaFin = inv.FechaFin;
                                respuesta.UsuarioMod = inv.UsuarioMod;
                                respuesta.UsuarioReg = inv.UsuarioReg;

                                FicLoBDContext.Update(respuesta);

                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-UPDATE-> IdCarreraEspecilidad: " + inv.IdCarreraEspecilidad + " \n" : "-NO NECESITO ACTUALIZAR-> IdCarreraEspecilidad: " + inv.IdCarreraEspecilidad + " \n";
                                FicLoBDContext.Entry<eva_carreras_especialidades>(respuesta).State = EntityState.Detached;
                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                        else
                        {
                            try
                            {
                                FicLoBDContext.Add(inv);
                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-INSERT-> IdCarreraEspecilidad: " + inv.IdCarreraEspecilidad + " \n" : "-ERROR EN INSERTAR-> IdCarreraEspecilidad: " + inv.IdCarreraEspecilidad + " \n";
                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                    }
                }
                else FicMensaje += "-> SIN DATOS. \n";

                if (FicGetReultREST != null && FicGetReultREST.eva_cat_reticulas != null)
                {
                    FicMensaje += "IMPORTANDO: eva_cat_reticulas \n";
                    foreach (eva_cat_reticulas inv in FicGetReultREST.eva_cat_reticulas)
                    {
                        var respuesta = await FicExistzt_eva_cat_reticulas(inv.IdReticula);
                        if (respuesta != null)
                        {
                            try
                            {
                                respuesta.IdReticula = inv.IdReticula;
                                respuesta.idGenPlanEstudios = inv.idGenPlanEstudios;
                                respuesta.IdTipoGenPlanEstudios = inv.IdTipoGenPlanEstudios;
                                respuesta.Clave = inv.Clave;
                                respuesta.DesReticula = inv.DesReticula;
                                respuesta.Actual = inv.Actual;
                                respuesta.Activo = inv.Activo;
                                respuesta.Borrado = inv.Borrado;
                                respuesta.FechaReg = inv.FechaReg;
                                respuesta.FechaUltMod = inv.FechaUltMod;
                                respuesta.FechaIni = inv.FechaIni;
                                respuesta.FechaFin = inv.FechaFin;
                                respuesta.UsuarioMod = inv.UsuarioMod;
                                respuesta.UsuarioReg = inv.UsuarioReg;
                              
                                FicLoBDContext.Update(respuesta);

                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-UPDATE-> IdReticula: " + inv.IdReticula + " \n" : "-NO NECESITO ACTUALIZAR-> IdReticula: " + inv.IdReticula + " \n";
                                FicLoBDContext.Entry<eva_cat_reticulas>(respuesta).State = EntityState.Detached;
                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                        else
                        {
                            try
                            {
                                FicLoBDContext.Add(inv);
                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-INSERT-> IdReticula: " + inv.IdReticula + " \n" : "-ERROR EN INSERTAR-> IdReticula: " + inv.IdReticula + " \n";
                                FicLoBDContext.Entry<eva_cat_reticulas>(inv).State = EntityState.Detached;

                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                    }
                }
                else FicMensaje += "-> SIN DATOS. \n";

                if (FicGetReultREST != null && FicGetReultREST.eva_carreras_reticulas != null)
                {
                    FicMensaje += "IMPORTANDO: eva_carreras_reticulas \n";
                    foreach (eva_carreras_reticulas inv in FicGetReultREST.eva_carreras_reticulas)
                    {
                        var respuesta = await FicExistzt_eva_carreras_reticulas(inv.IdReticula);
                        if (respuesta != null)
                        {
                            try
                            {
                                respuesta.IdCarreraRiticula = inv.IdCarreraRiticula;
                                respuesta.IdCarrera = inv.IdCarrera;
                                respuesta.IdReticula = inv.IdReticula;
                                respuesta.Activo = inv.Activo;
                                respuesta.Borrado = inv.Borrado;
                                respuesta.FechaReg = inv.FechaReg;
                                respuesta.FechaUltMod = inv.FechaUltMod;
                                respuesta.UsuarioMod = inv.UsuarioMod;
                                respuesta.UsuarioReg = inv.UsuarioReg;
                            
                                FicLoBDContext.Update(respuesta);

                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-UPDATE-> IdCarreraRiticula: " + inv.IdCarreraRiticula + " \n" : "-NO NECESITO ACTUALIZAR-> IdCarreraRiticula: " + inv.IdCarreraRiticula + " \n";
                                FicLoBDContext.Entry<eva_carreras_reticulas>(respuesta).State = EntityState.Detached;
                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                        else
                        {
                            try
                            {
                                FicLoBDContext.Add(inv);
                                FicMensaje += await FicLoBDContext.SaveChangesAsync() > 0 ? "-INSERT-> IdCarreraRiticula: " + inv.IdCarreraRiticula + " \n" : "-ERROR EN INSERTAR-> IdCarreraRiticula: " + inv.IdCarreraRiticula + " \n";
                                FicLoBDContext.Entry<eva_carreras_reticulas>(inv).State = EntityState.Detached;

                            }
                            catch (Exception e)
                            {
                                FicMensaje += "-ALERTA-> " + e.Message.ToString() + " \n";
                            }
                        }
                    }
                }
                else FicMensaje += "-> SIN DATOS. \n";

            }
            catch (Exception e)
            {
                FicMensaje += "ALERTA: " + e.Message.ToString() + "\n";
            }
            return FicMensaje;
        }//FicGetImportCatalogos()
    }
}
