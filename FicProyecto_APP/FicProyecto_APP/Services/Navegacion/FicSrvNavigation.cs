﻿using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Acerca;
using FicProyecto_APP.ViewModels.Carreras;
using FicProyecto_APP.ViewModels.Especialidades;
using FicProyecto_APP.ViewModels.Especialidades.Carreras;
using FicProyecto_APP.ViewModels.Generales;
using FicProyecto_APP.ViewModels.ImportarExportar;
using FicProyecto_APP.Views.Acerca;
using FicProyecto_APP.Views.Carreras;
using FicProyecto_APP.Views.Especialidades;
using FicProyecto_APP.Views.Especialidades.Carreras;
using FicProyecto_APP.Views.Generales;
using FicProyecto_APP.Views.WebApi;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace FicProyecto_APP.Services.Navegacion
{
    class FicSrvNavigation : IFicSrvNavigation
    {
        private IDictionary<Type, Type> FicViewModelRouting = new Dictionary<Type, Type>()
        { 
            //AQUI SE HACE UNA UNION ENTRE LA VM Y VI DE CADA VIEW DE LA APP
       
           /*ACERCA DE*/
            {typeof(FicVmAcerca), typeof(FicViAcercaDe)},

            /*GENERALES*/
            {typeof(FicVmGenerales), typeof(FicViGeneralesList)},
            {typeof(FicVmGeneralesTipo), typeof(FicViGeneralesTipoList)},

            /*CARRERAS */
            {typeof(FicVmCatCarreras), typeof(FicViCarrerasList)},
            {typeof(FicVmCatCarrerasAdd), typeof(FicViCarrerasAdd)},
            {typeof(FicVmCatCarrerasUpdate), typeof(FicViCarrerasUpdate)},
            {typeof(FicVmCatCarrerasDetail), typeof(FicViCarrerasDetail)},

            /*IMPORTTAR*/
             {typeof(FicVmImportarWebApi), typeof(FicViImportarWebApi)},
              {typeof(FicVmExportarWebApi), typeof(FicViExportarWebApi)},

             /*ESPECIALIDADES */
             {typeof(FicVmCatEspecialidades), typeof(FicViEspecialidadesList)},
             {typeof(FicVmCatEspecialidadesInsertar), typeof(FicViEspecialidadesInsertar)},
             {typeof(FicVmCatEspecialidadesUpdate), typeof(FicViEspecialidadesUpdate)},
             {typeof(FicVmCatEspecialidadesDetalle), typeof(FicViEspecialidadesDetalle)},

             {typeof(FicVmCarrerasEspecialidades), typeof(FicViEspecialidadesCarreraList)},
             {typeof(FicVmCarrerasEspecialidadesInsertar), typeof(FicViEspecialidadesCarreraInsertar)},
             {typeof(FicVmCarrerasEspecialidadesUpdate), typeof(FicViEspecialidadesCarreraUpdate)},
             {typeof(FicVmCarrerasEspecialidadesDetalle), typeof(FicViEspecialidadesCarreraDetalle)},

             {typeof(FicVmEspeCarrera), typeof(FicViEspeCarreraList)},
             {typeof(FicVmEspeCarreraAdd), typeof(FicViEspeCarreraAgregar)},

        };

        #region METODOS DE IMPLEMENTACION DE LA INTERFACE -> IFicSrvNavigationInventario

        public void FicMetNavigateTo<FicTDestinationViewModel>(object FicNavigationContext = null)
        {
            Type FicPageType = FicViewModelRouting[typeof(FicTDestinationViewModel)];
            var FicPage = Activator.CreateInstance(FicPageType, FicNavigationContext) as Page;

            if (FicPage != null)
            {
                var mdp = Application.Current.MainPage as MasterDetailPage;
                mdp.Detail.Navigation.PushAsync(FicPage);
            }
        }
        public void FicMetNavigateTo<TDestinationViewModel>(object navigationContext = null, bool show = true)
        {
            Type pageType = FicViewModelRouting[typeof(TDestinationViewModel)];
            var page = Activator.CreateInstance(pageType, navigationContext, show) as Page;

            if (page != null)
                Application.Current.MainPage.Navigation.PushAsync(page);
        }

        public void FicMetNavigateTo(Type FicDestinationType, object FicNavigationContext = null)
        {
            Type FicPageType = FicViewModelRouting[FicDestinationType];
            var FicPage = Activator.CreateInstance(FicPageType, FicNavigationContext) as Page;
            if (FicPage != null)
            {
                var mdp = Application.Current.MainPage as MasterDetailPage;
                mdp.Detail.Navigation.PushAsync(FicPage);
            }
        }

        public void FicMetNavigateBack()
        {
#if __ANDROID__
            Application.Current.MainPage.Navigation.PopAsync(true);
#endif

            Application.Current.MainPage.Navigation.PopAsync(true);
            // new NavigationPage(new FicViCatEdificiosList());


        }
        #endregion

    }
}
