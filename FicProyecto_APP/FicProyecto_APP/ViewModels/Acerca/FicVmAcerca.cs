﻿using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace FicProyecto_APP.ViewModels.Acerca
{
    public class FicVmAcerca : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;
        public FicVmAcerca(IFicSrvNavigation FicPaSrvNavigation)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;

        }

        public override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
        }
    }
}
