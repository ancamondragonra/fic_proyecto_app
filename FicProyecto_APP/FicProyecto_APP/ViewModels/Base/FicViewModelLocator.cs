﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using FicProyecto_APP.Interfaces.Carreras;
using FicProyecto_APP.Interfaces.Especialidades;
using FicProyecto_APP.Interfaces.Generales;
using FicProyecto_APP.Interfaces.ImportarExportar;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.Services.Carreras;
using FicProyecto_APP.Services.Especialidade;
using FicProyecto_APP.Services.Generales;
using FicProyecto_APP.Services.ImportarExportar;
using FicProyecto_APP.Services.Navegacion;
using FicProyecto_APP.ViewModels.Acerca;
using FicProyecto_APP.ViewModels.Carreras;
using FicProyecto_APP.ViewModels.Especialidades;
using FicProyecto_APP.ViewModels.Especialidades.Carreras;
using FicProyecto_APP.ViewModels.Generales;
using FicProyecto_APP.ViewModels.ImportarExportar;

namespace FicProyecto_APP.ViewModels.Base
{
    public class FicViewModelLocator
    {
        private static IContainer FicIContainer;

        public FicViewModelLocator()
        {
            //FIC: ContainerBuilder es una clase de la libreria de Autofac para poder ejecutar la interfaz en las diferentes plataformas 
            var FicContainerBuilder = new ContainerBuilder();


            FicContainerBuilder.RegisterType<FicVmAcerca>();
            /*IMPORTAR EXPORTAR*/
            FicContainerBuilder.RegisterType<FicVmImportarWebApi>();
            FicContainerBuilder.RegisterType<FicVmExportarWebApi>();

            /*CARRERAS*/
            FicContainerBuilder.RegisterType<FicVmCatCarreras>();
            FicContainerBuilder.RegisterType<FicVmCatCarrerasAdd>();
            FicContainerBuilder.RegisterType<FicVmCatCarrerasUpdate>();
            FicContainerBuilder.RegisterType<FicVmCatCarrerasDetail>();

            /*ESPECIAIDADES*/
            FicContainerBuilder.RegisterType<FicVmCatEspecialidades>();
            FicContainerBuilder.RegisterType<FicVmCatEspecialidadesInsertar>();
            FicContainerBuilder.RegisterType<FicVmCatEspecialidadesUpdate>();
            FicContainerBuilder.RegisterType<FicVmCatEspecialidadesDetalle>();
            
            FicContainerBuilder.RegisterType<FicVmCarrerasEspecialidades>();
            FicContainerBuilder.RegisterType<FicVmCarrerasEspecialidadesInsertar>();
            FicContainerBuilder.RegisterType<FicVmCarrerasEspecialidadesUpdate>();
            FicContainerBuilder.RegisterType<FicVmCarrerasEspecialidadesDetalle>();

            FicContainerBuilder.RegisterType<FicVmEspeCarrera>();
            FicContainerBuilder.RegisterType<FicVmEspeCarreraAdd>();

            /*GENERAES*/
            FicContainerBuilder.RegisterType<FicVmGenerales>();
            FicContainerBuilder.RegisterType<FicVmGeneralesTipo>();

            /*INTERFACES UNION SERVICIOS*/

            FicContainerBuilder.RegisterType<FicSrvNavigation>().As<IFicSrvNavigation>().SingleInstance();

            FicContainerBuilder.RegisterType<FicSrvCatCarrerasList>().As<IFicSrvCatCarreras>();
            FicContainerBuilder.RegisterType<FicSrvCatEspecialidades>().As<IFicSrvCatEspecialidades>();
            FicContainerBuilder.RegisterType<FicSrvCatGenerales>().As<IFicSrvGenerales>();

            FicContainerBuilder.RegisterType<FicSrvExportarWebApi>().As<IFicSrvExportarWebApi>();
            FicContainerBuilder.RegisterType<FicSrvImportarWebApi>().As<IFicSrvImportarWebApi>();

          
     

            //FIC: se asigna o se libera el contenedor
            //-------------------------------------------
            if (FicIContainer != null)
            {

                FicIContainer.Dispose();
            }

            FicIContainer = FicContainerBuilder.Build();
        }//CONSTRUCTOR


        /*-Especialidades-*/
        public FicVmCatEspecialidades FicVmCatEspecialidades
        {
            get { return FicIContainer.Resolve<FicVmCatEspecialidades>(); }
        }
        public FicVmCatEspecialidadesInsertar FicVmCatEspecialidadesInsertar
        {
            get { return FicIContainer.Resolve<FicVmCatEspecialidadesInsertar>(); }
        }
        public FicVmCatEspecialidadesDetalle FicVmCatEspecialidadesDetalle
        {
            get { return FicIContainer.Resolve<FicVmCatEspecialidadesDetalle>(); }
        }
        public FicVmCatEspecialidadesUpdate FicVmCatEspecialidadesUpdate
        {
            get { return FicIContainer.Resolve<FicVmCatEspecialidadesUpdate>(); }
        }
        

        public FicVmCarrerasEspecialidades FicVmCarrerasEspecialidades
        {
            get { return FicIContainer.Resolve<FicVmCarrerasEspecialidades>(); }
        }
        public FicVmCarrerasEspecialidadesDetalle FicVmCarrerasEspecialidadesDetalle
        {
            get { return FicIContainer.Resolve<FicVmCarrerasEspecialidadesDetalle>(); }
        }
        public FicVmCarrerasEspecialidadesInsertar FicVmCarrerasEspecialidadesInsertar
        {
            get { return FicIContainer.Resolve<FicVmCarrerasEspecialidadesInsertar>(); }
        }
        public FicVmCarrerasEspecialidadesUpdate FicVmCarrerasEspecialidadesUpdate
        {
            get { return FicIContainer.Resolve<FicVmCarrerasEspecialidadesUpdate>(); }
        }

        
        public FicVmEspeCarrera FicVmEspeCarrera
        {
            get { return FicIContainer.Resolve<FicVmEspeCarrera>(); }
        }
        public FicVmEspeCarreraAdd FicVmEspeCarreraAdd
        {
            get { return FicIContainer.Resolve<FicVmEspeCarreraAdd>(); }
        }
        
        /*-Impotar y expotart-*/
        public FicVmImportarWebApi FicVmImportarWebApi
        {
            get { return FicIContainer.Resolve<FicVmImportarWebApi>(); }
        }
        public FicVmExportarWebApi FicVmExportarWebApi
        {
            get { return FicIContainer.Resolve<FicVmExportarWebApi>(); }
        }
     

        /*-CARRERAS-*/
        public FicVmCatCarreras FicVmCatCarreras
        {
            get { return FicIContainer.Resolve<FicVmCatCarreras>(); }
        }
        public FicVmCatCarrerasAdd FicVmCatCarrerasAdd
        {
            get { return FicIContainer.Resolve<FicVmCatCarrerasAdd>(); }
        }
        public FicVmCatCarrerasDetail FicVmCatCarrerasDetail
        {
            get { return FicIContainer.Resolve<FicVmCatCarrerasDetail>(); }
        }
        public FicVmCatCarrerasUpdate FicVmCatCarrerasUpdate
        {
            get { return FicIContainer.Resolve<FicVmCatCarrerasUpdate>(); }
        }


        /*-GENERALES-*/
        public FicVmGenerales FicVmGenerales
        {
            get { return FicIContainer.Resolve<FicVmGenerales>(); }
        }
        public FicVmGeneralesTipo FicVmGeneralesTipo
        {
            get { return FicIContainer.Resolve<FicVmGeneralesTipo>(); }
        }
    }

}
