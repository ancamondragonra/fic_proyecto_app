﻿using FicProyecto_APP.Interfaces.Carreras;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using FicProyecto_APP.ViewModels.Especialidades;
using FicProyecto_APP.ViewModels.Especialidades.Carreras;
using FicProyecto_APP.Views.Especialidades;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;


namespace FicProyecto_APP.ViewModels.Carreras
{
    public  class FicVmCatCarreras : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvCatCarreras FicLoSrvApp;//INTERFAZ CRUD
        private string _FicLabelIdInv;

        public FicVmCatCarreras(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatCarreras FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }

        public ObservableCollection<eva_cat_carreras> _SfDataGrid_ItemSource_Carreras;
        public ObservableCollection<eva_cat_carreras> SfDataGrid_ItemSource_Carreras
        {
            get { return _SfDataGrid_ItemSource_Carreras; }
            set
            {
                _SfDataGrid_ItemSource_Carreras = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<eva_cat_carreras> _SfDataGrid_ItemSource_CarrerasBusqueda;
        public ObservableCollection<eva_cat_carreras> SfDataGrid_ItemSource_CarrerasBusqueda
        {
            get { return _SfDataGrid_ItemSource_CarrerasBusqueda; }
            set
            {
                _SfDataGrid_ItemSource_CarrerasBusqueda = value;
                RaisePropertyChanged();
            }
        }


        private eva_cat_carreras _SfDataGrid_SelectItem_Carreras;
        public eva_cat_carreras SfDataGrid_SelectItem_Carreras
        {
            get { return _SfDataGrid_SelectItem_Carreras; }
            set
            {
                _SfDataGrid_SelectItem_Carreras = value;
                RaisePropertyChanged();
            }
        }

        public async override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            var resultadoEdificios = await FicLoSrvApp.FicMetGetListCarreras();
            SfDataGrid_ItemSource_Carreras = new ObservableCollection<eva_cat_carreras>();
            foreach (var edifcio in resultadoEdificios)
            {
                SfDataGrid_ItemSource_Carreras.Add(edifcio);
            }
            SfDataGrid_ItemSource_CarrerasBusqueda = new ObservableCollection<eva_cat_carreras>();
            foreach (var item in SfDataGrid_ItemSource_Carreras)
            {
                SfDataGrid_ItemSource_CarrerasBusqueda.Add(item);
            }
        }

        private ICommand AddNewCarrera;
        public ICommand FicMetAddCommand
        {
            get { return AddNewCarrera = AddNewCarrera ?? new FicVmDelegateCommand(AddNewEdificioExecute); }
        }
        private void AddNewEdificioExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatCarrerasAdd>(null);
        }

        
        private ICommand EditCarrera;
        public ICommand FicMetEditCommand
        {
            get { return EditCarrera = EditCarrera ?? new FicVmDelegateCommand(EditEdificioExecute); }
        }
        private void EditEdificioExecute()
        {
            var temp = _SfDataGrid_SelectItem_Carreras;
            if (temp != null)
            {
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCatCarrerasUpdate>(temp);
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Alerta", "Por favor seleccione un item para poder modificarlo. ", "OK");
            }
        }
        


        private ICommand DetailsCarreras;
        public ICommand FicMetDetailsCommand
        {
            get { return DetailsCarreras = DetailsCarreras ?? new FicVmDelegateCommand(DetailsEdificioExecute); }
        }
        private void DetailsEdificioExecute()
        {
            var temp = _SfDataGrid_SelectItem_Carreras;
            if (temp != null)
            {
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCatCarrerasDetail>(temp);
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Alerta", "Por favor seleccione una Carrera. ", "OK");
            }

        }

        public string FicLabelIdInv
        {
            get { return _FicLabelIdInv; }
            set
            {
                if (value != null) _FicLabelIdInv = value;
                if (value == string.Empty)
                {
                    SfDataGrid_ItemSource_Carreras.Clear();
                    foreach (var item in SfDataGrid_ItemSource_CarrerasBusqueda)
                    {
                        SfDataGrid_ItemSource_Carreras.Add(item);
                    }
                }  
            }
        }


        private ICommand SearchCommand;
        public ICommand SearchCommandC
        {
            get { return SearchCommand = SearchCommand ?? new FicVmDelegateCommand(SearchCommandExecute); }
        }
        private void SearchCommandExecute()
        {
            if (FicLabelIdInv!="")
            {
                var tempRecords = SfDataGrid_ItemSource_CarrerasBusqueda.Where(
                    c => c.DesCarrera.Contains(FicLabelIdInv) ||
                    c.Alias.Contains(FicLabelIdInv) || 
                    c.ClaveCarrera.Contains(FicLabelIdInv) ||
                    c.cat_generales.DesGeneral.Contains(FicLabelIdInv) ||
                    c.cat_tipos_generales.DesTipo.Contains(FicLabelIdInv)
                    );
                
                SfDataGrid_ItemSource_Carreras.Clear();
                foreach (var item in tempRecords)
                {
                    SfDataGrid_ItemSource_Carreras.Add(item);
                }
            }
            else
            {
                SfDataGrid_ItemSource_Carreras.Clear();
                foreach (var item in SfDataGrid_ItemSource_CarrerasBusqueda)
                {
                    SfDataGrid_ItemSource_Carreras.Add(item);
                }
            }
        }

        private ICommand DeleteCarrera;
        public ICommand FicMetDeleteCommand
        {
            get { return DeleteCarrera = DeleteCarrera ?? new FicVmDelegateCommand(DeleteEdificioExecute); }
        }
        private async void DeleteEdificioExecute()
        {
            var temp = _SfDataGrid_SelectItem_Carreras;
            if (temp != null)
            {
                var accion = await Application.Current.MainPage.DisplayActionSheet("Eliminar la carrera  " + temp.DesCarrera, "Cancel", null, "Si", "No");
                if (accion.Equals("Si"))
                {
                    eva_cat_carreras t = new eva_cat_carreras();
                    t.IdCarrera = temp.IdCarrera;
                    await FicLoSrvApp.FicMetRemoveCarrera(t);
                    FicLoSrvNavigation.FicMetNavigateTo<FicVmCatCarreras>(null);
                }
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Alerta", "Por favor seleccione una carrera para poder eliminarlo. ", "OK");
            }
        }


        private ICommand ShowEspecialidades;
        public ICommand ShowEspecialidadesCommand
        {
            get { return ShowEspecialidades = ShowEspecialidades ?? new FicVmDelegateCommand(ShowEspecialidadesEX); }
        }
        private void ShowEspecialidadesEX()
        {
           FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEspecialidades>(null);
        }
        private ICommand ShowEspecialidadesCarreras;
        public ICommand ShowEspecialidades2Command
        {
            get { return ShowEspecialidadesCarreras = ShowEspecialidadesCarreras ?? new FicVmDelegateCommand(ShowEspecialidadesEX2); }
        }
        private void ShowEspecialidadesEX2()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCarrerasEspecialidades>(null);
        }

        private ICommand ShowReticulas;
        public ICommand ShowReticulasCommand
        {
            get { return ShowReticulas = ShowReticulas ?? new FicVmDelegateCommand(ShowReticulasExecute); }
        }
        private void ShowReticulasExecute()
        {
           // FicLoSrvNavigation.FicMetNavigateTo<FicVmCarrerasEspecialidades>(null);
        }

        private ICommand CommandCarreraEspe;
        public ICommand ShowEspecialidadesCarreraCommand
        {
            get { return CommandCarreraEspe = CommandCarreraEspe ?? new FicVmDelegateCommand(CommandCarreraExecute); }
        }
        private void CommandCarreraExecute()
        {
            var temp = _SfDataGrid_SelectItem_Carreras;
            if (temp != null)
            {
                FicLoSrvNavigation.FicMetNavigateTo<FicVmEspeCarrera>(temp);
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Alerta", "Por favor seleccione un item para poder ver sus especialidades. ", "OK");
            }
        }

    }
}
