﻿using FicProyecto_APP.Data;
using FicProyecto_APP.Interfaces.Carreras;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;


namespace FicProyecto_APP.ViewModels.Carreras
{
    public  class FicVmCatCarrerasAdd : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvCatCarreras FicLoSrvApp;//INTERFAZ CRUD

        public FicVmCatCarrerasAdd(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatCarreras FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }

        public async override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            _Carreras = new eva_cat_carreras();

            Carreras.Activo = "False";
            Carreras.Borrado = "False";
            Carreras.FechaIni = DateTime.Now;
            Carreras.FechaFin = DateTime.Now;

            var resultadoGrado = await FicLoSrvApp.GetGeneralesByTipo(17);
            var resultadoModalidad = await FicLoSrvApp.GetGeneralesByTipo(18);
            cat_generales_grado = new ObservableCollection<cat_generales>();
           
            foreach (var temp in resultadoGrado)
            {
                cat_generales_grado.Add(temp);
            }
            cat_generales_modalidad = new ObservableCollection<cat_generales>();
            foreach (var temp in resultadoModalidad)
            {
                cat_generales_modalidad.Add(temp);
            }

        }


        private eva_cat_carreras _Carreras;
        public eva_cat_carreras Carreras
        {
            get { return _Carreras; }
            set
            {
                _Carreras = value;
                RaisePropertyChanged();
            }
        }

  
        private ICommand BackNavigation;
        public ICommand BackNavgCommand
        {
            get { return BackNavigation = BackNavigation ?? new FicVmDelegateCommand(BackNavgExecute); }
        }
        private void BackNavgExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatCarreras>(null);
        }


        public ObservableCollection<cat_generales> _cat_generales_grado;
        public ObservableCollection<cat_generales> cat_generales_grado
        {
            get { return _cat_generales_grado; }
            set
            {
                _cat_generales_grado = value;
                RaisePropertyChanged();
            }
        }
        public ObservableCollection<cat_generales> _cat_generales_modalidad;
        public ObservableCollection<cat_generales> cat_generales_modalidad
        {
            get { return _cat_generales_modalidad; }
            set
            {
                _cat_generales_modalidad = value;
                RaisePropertyChanged();
            }
        }


        private cat_generales _Selectedcat_generalesGrado;
        public cat_generales Selectedcat_generalesGrado
        {
            get { return _Selectedcat_generalesGrado; }
            set
            {
                _Selectedcat_generalesGrado = value;
                RaisePropertyChanged();
            }
        }

        private cat_generales _Selectedcat_generalesModalidad;
        public cat_generales Selectedcat_generalesModalidad
        {
            get { return _Selectedcat_generalesModalidad; }
            set
            {
                _Selectedcat_generalesModalidad = value;
                RaisePropertyChanged();
            }
        }


        private ICommand AddCarrera;
        public ICommand FicMetAddCommand
        {
            get { return AddCarrera = AddCarrera ?? new FicVmDelegateCommand(AddExecute); }
        }
        private void AddExecute()
        {
        

            if (
                (Carreras.ClaveCarrera==null) || (Carreras.ClaveOficial == null) || (Carreras.DesCarrera == null )
                || (Carreras.Alias == null) || (Selectedcat_generalesGrado == null) || (Selectedcat_generalesModalidad == null)
                  || (DateTime.Compare(Carreras.FechaIni.Value.Date, Carreras.FechaFin.Value.Date) == 1)
                )
            {
                string mensaje= " \n";
                if (DateTime.Compare(Carreras.FechaIni.Value.Date, Carreras.FechaFin.Value.Date)==1)
                {
                    mensaje += "Fecha final debes ser mayor que la inicial \n";
                }
                if (Carreras.ClaveCarrera==null){
                    mensaje+="Clave carrera vacia \n";
                }
                if(Carreras.ClaveOficial==null){
                    mensaje+="Clave Oficial vacia \n";
                }
                 if(Carreras.DesCarrera==null){
                    mensaje+="Descripcion Carrera vacia \n";
                }
                 if(Carreras.Alias==null){
                    mensaje+="Alias vacia \n";
                }
                 if(Selectedcat_generalesGrado==null){
                    mensaje+="Sin grado vacia \n";
                }
                 if(Selectedcat_generalesModalidad==null){
                    mensaje+="Sin Modalidad vacia \n";
                }
                Application.Current.MainPage.DisplayAlert("Alerta", "Campos vacios en "+mensaje+"", "OK");
            }
            else
            {
                Carreras.IdGenGradoEscolar = Selectedcat_generalesGrado.IdGeneral;
                Carreras.IdTipoGenGradoEscolar = Selectedcat_generalesGrado.IdTipoGeneral;
                Carreras.IdGenModalidad = Selectedcat_generalesModalidad.IdGeneral;
                Carreras.IdTipoGenModalidad = Selectedcat_generalesModalidad.IdTipoGeneral;
                Carreras.FechaReg = DateTime.Now;
                Carreras.FechaUltMod = DateTime.Now;
                Carreras.UsuarioReg= "FicIbarra";
                Carreras.UsuarioMod= Carreras.UsuarioReg;
                if (Carreras.Activo.Equals("True"))
                {
                    Carreras.Activo = "S";
                }
                else
                {
                    Carreras.Activo = "N";
                }
                if (Carreras.Borrado.Equals("True"))
                {
                    Carreras.Borrado = "S";
                }
                else
                {
                    Carreras.Borrado = "N";
                }
                var maxid = FicLoSrvApp.FicMetGetMaxCarrerasId();
                try
                {
                    if (maxid.Result == null)
                    {
                        Carreras.IdCarrera = 1;
                    }
                    else
                    {
                        Carreras.IdCarrera = (Int16)(maxid.Result.IdCarrera + 1);
                    }
                }
                catch (System.AggregateException e)
                {
                    Carreras.IdCarrera = 1;
                }
                Carreras.cat_generales = null;
                Carreras.cat_tipos_generales = null;
                Carreras.cat_generales_modalidad = null;
                Carreras.cat_tipos_generales_modalidad = null;
                FicLoSrvApp.FicMetInsertCarrera(Carreras);
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCatCarreras>(null);
            } 
        }

    }
}
