﻿using FicProyecto_APP.Interfaces.Carreras;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;


namespace FicProyecto_APP.ViewModels.Carreras
{
    public  class FicVmCatCarrerasDetail : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvCatCarreras FicLoSrvApp;//INTERFAZ CRUD

        private eva_cat_carreras _Carrera;
        public eva_cat_carreras Carrera
        {
            get { return _Carrera; }
            set
            {
                _Carrera = value;
                RaisePropertyChanged();
            }
        }


        private ICommand BackNavigation;
        public ICommand BackNavgCommand
        {
            get { return BackNavigation = BackNavigation ?? new FicVmDelegateCommand(BackNavgExecute); }
        }
        public void BackNavgExecute()
        {
            //FicLoSrvNavigation.FicMetNavigateBack();
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatCarreras>(null);
        }


        private ICommand EditCarrera;
        public ICommand FicMetEditCommand
        {
            get { return EditCarrera = EditCarrera ?? new FicVmDelegateCommand(EditCarreraExecute); }
        }
        private void EditCarreraExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatCarrerasUpdate>(Carrera);
        }


        public FicVmCatCarrerasDetail(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatCarreras FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }

        public override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            Carrera = navigationContext as eva_cat_carreras;
        }

    }
}
