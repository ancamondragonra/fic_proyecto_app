﻿using FicProyecto_APP.Interfaces.Especialidades;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using static FicProyecto_APP.Models.FicCarrera;
using System.Linq;
using System.Windows.Input;
using FicProyecto_APP.ViewModels.Carreras;
using Xamarin.Forms;

namespace FicProyecto_APP.ViewModels.Especialidades.Carreras
{
    public class FicVmEspeCarrera : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvCatEspecialidades FicLoSrvApp;//INTERFAZ CRUD
        private string _FicLabelIdInv;

        public FicVmEspeCarrera(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatEspecialidades FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }

        public string FicLabelIdInv
        {
            get { return _FicLabelIdInv; }
            set
            {
                if (value != null) _FicLabelIdInv = value;
                if (value == string.Empty)
                {
                    SfDataGrid_ItemSource_Data.Clear();
                    foreach (var item in SfDataGrid_ItemSource_DataB)
                    {
                        SfDataGrid_ItemSource_Data.Add(item);
                    }
                }
            }
        }
        public ObservableCollection<eva_cat_especialidades> _SfDataGrid_ItemSource_Data;
        public ObservableCollection<eva_cat_especialidades> SfDataGrid_ItemSource_Data
        {
            get { return _SfDataGrid_ItemSource_Data; }
            set
            {
                _SfDataGrid_ItemSource_Data = value;
                RaisePropertyChanged();
            }
        }
        public ObservableCollection<eva_cat_especialidades> _SfDataGrid_ItemSource_DataB;
        public ObservableCollection<eva_cat_especialidades> SfDataGrid_ItemSource_DataB
        {
            get { return _SfDataGrid_ItemSource_DataB; }
            set
            {
                _SfDataGrid_ItemSource_DataB = value;
                RaisePropertyChanged();
            }
        }
        private eva_cat_especialidades _SfDataGrid_SelectItem_Data;
        public eva_cat_especialidades SfDataGrid_SelectItem_Data
        {
            get { return _SfDataGrid_SelectItem_Data; }
            set
            {
                _SfDataGrid_SelectItem_Data = value;
                RaisePropertyChanged();
            }
        }

        private ICommand SearchCommand;
        public ICommand SearchCommandC
        {
            get { return SearchCommand = SearchCommand ?? new FicVmDelegateCommand(SearchCommandExecute); }
        }
        private void SearchCommandExecute()
        {
            if (FicLabelIdInv != "")
            {
                var tempRecords = SfDataGrid_ItemSource_DataB.Where(
                    c => c.DesEspecialidad.Contains(FicLabelIdInv) 
                    );

                SfDataGrid_ItemSource_Data.Clear();
                foreach (var item in tempRecords)
                {
                    SfDataGrid_ItemSource_Data.Add(item);
                }
            }
            else
            {
                SfDataGrid_ItemSource_Data.Clear();
                foreach (var item in SfDataGrid_ItemSource_DataB)
                {
                    SfDataGrid_ItemSource_Data.Add(item);
                }
            }
        }

        private ICommand FicMetBack;
        public ICommand FicMetBackCommand
        {
            get { return FicMetBack = FicMetBack ?? new FicVmDelegateCommand(BACKExecute); }
        }
        private void BACKExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatCarreras>(null);
        }

        private ICommand AddNew;
        public ICommand FicMetAddCommand
        {
            get { return AddNew = AddNew ?? new FicVmDelegateCommand(AddNewExecute); }
        }
        private void AddNewExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmEspeCarreraAdd>(temp);
        }


        private ICommand DeleteCommand;
        public ICommand FicMetDeleteCommand
        {
            get { return DeleteCommand = DeleteCommand ?? new FicVmDelegateCommand(DeleteExecute); }
        }
        private async void DeleteExecute()
        {
            var tempa = SfDataGrid_SelectItem_Data;
            if (tempa != null)
            {
                var accion = await Application.Current.MainPage.DisplayActionSheet("Eliminar   " + tempa.DesEspecialidad, "Cancel", null, "Si", "No");
                if (accion.Equals("Si"))
                {
                    var rw = await FicLoSrvApp.FicMetGetListEspecialidadesByCarrera2(temp.IdCarrera, tempa.IdEspecialidad);

                    eva_carreras_especialidades t = new eva_carreras_especialidades();
                    t.IdCarreraEspecilidad = rw.IdCarreraEspecilidad;
                    await FicLoSrvApp.FicMetRemoveCarrera_Especialidades(t);
                    FicLoSrvNavigation.FicMetNavigateTo<FicVmEspeCarrera>(temp);
                }
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Alerta", "Por favor seleccione un item para poder eliminarlo. ", "OK");
            }
        }

        public eva_cat_carreras temp = new eva_cat_carreras();
        public async override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            temp =  navigationContext as eva_cat_carreras;
            var rw = await FicLoSrvApp.FicMetGetListEspecialidadesByCarrera(temp.IdCarrera);

            SfDataGrid_ItemSource_Data = new ObservableCollection<eva_cat_especialidades>();
            foreach (var c in rw)
            {
                SfDataGrid_ItemSource_Data.Add(c);
            }
            SfDataGrid_ItemSource_DataB = new ObservableCollection<eva_cat_especialidades>();
            foreach (var c in SfDataGrid_ItemSource_Data)
            {
                SfDataGrid_ItemSource_DataB.Add(c);
            }
        }
    }
}
