﻿using FicProyecto_APP.Interfaces.Especialidades;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;

namespace FicProyecto_APP.ViewModels.Especialidades.Carreras
{
    public class FicVmEspeCarreraAdd : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;
        private IFicSrvCatEspecialidades FicLoSrvApp;

        public FicVmEspeCarreraAdd(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatEspecialidades FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }

     
        private eva_cat_especialidades _SelectedEspecialidad;
        public eva_cat_especialidades SelectedEspecialidad
        {
            get { return _SelectedEspecialidad; }
            set
            {
                _SelectedEspecialidad = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<eva_cat_especialidades> _Data_Especialidades;
        public ObservableCollection<eva_cat_especialidades> Data_Especialidades
        {
            get { return _Data_Especialidades; }
            set
            {
                _Data_Especialidades = value;
                RaisePropertyChanged();
            }
        }

        private eva_carreras_especialidades _Data;
        public eva_carreras_especialidades Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                RaisePropertyChanged();
            }
        }

        public eva_cat_carreras temp1 = new eva_cat_carreras();
        public async override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            Data = new eva_carreras_especialidades();
            Data.Activo = "False";
            Data.Borrado = "False";
            Data.FechaIni = DateTime.Now;
            Data.FechaFin = DateTime.Now;

            temp1 = navigationContext as eva_cat_carreras;
            var especialidades = await FicLoSrvApp.FicMetGetListEspecialidades();
            Data_Especialidades = new ObservableCollection<eva_cat_especialidades>();
            foreach (var temp in especialidades)
            {
                Data_Especialidades.Add(temp);
            }
        }

        private ICommand BackNavigation;
        public ICommand BackNavgCommand
        {
            get { return BackNavigation = BackNavigation ?? new FicVmDelegateCommand(BackNavgExecute); }
        }
        private void BackNavgExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmEspeCarrera>(temp1);
        }

        private ICommand AddCarrera;
        public ICommand FicMetAddCommand
        {
            get { return AddCarrera = AddCarrera ?? new FicVmDelegateCommand(AddExecute); }
        }

        private void AddExecute()
        {
            if (
                (SelectedEspecialidad == null)
                  || (DateTime.Compare(Data.FechaIni.Value.Date, Data.FechaFin.Value.Date) == 1)
                )
            {
                string mensaje = " \n";
                if (DateTime.Compare(Data.FechaIni.Value.Date, Data.FechaFin.Value.Date) == 1)
                {
                    mensaje += "Fecha final debes ser mayor que la inicial \n";
                }
                if (SelectedEspecialidad == null)
                {

                    mensaje += "No se a seleccionado una especialidad \n";
                }
                Application.Current.MainPage.DisplayAlert("Alerta", "Campos vacios en " + mensaje + "", "OK");
            }
            else
            {
                if (Data.Activo.Equals("True"))
                {
                    Data.Activo = "S";
                }
                else
                {
                    Data.Activo = "N";
                }
                if (Data.Borrado.Equals("True"))
                {
                    Data.Borrado = "S";
                }
                else
                {
                    Data.Borrado = "N";
                }
                Data.UsuarioReg = "FicIbarra";
                Data.UsuarioMod = "FicIbarra";
                Data.FechaReg = DateTime.Now;
                Data.FechaUltMod = DateTime.Now;
                Data.IdCarrera = temp1.IdCarrera;
                Data.IdEspecialidad = SelectedEspecialidad.IdEspecialidad;
                var maxid = FicLoSrvApp.FicMetGetMaxCarrerasEspecialiddadIdAsync();
                try
                {
                    if (maxid.Result == null)
                    {
                        Data.IdCarreraEspecilidad = 1;
                    }
                    else
                    {
                        Data.IdCarreraEspecilidad = (Int16)(maxid.Result.IdCarreraEspecilidad + 1);
                    }
                }
                catch (System.AggregateException e)
                {
                    Data.IdCarreraEspecilidad = 1;
                }
                FicLoSrvApp.FicMetInsertCarreraEspecialidad(Data);
                FicLoSrvNavigation.FicMetNavigateTo<FicVmEspeCarrera>(temp1);
            }
        }
    }
}
