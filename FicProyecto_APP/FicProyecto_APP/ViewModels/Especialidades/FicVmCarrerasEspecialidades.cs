﻿using FicProyecto_APP.Interfaces.Especialidades;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using FicProyecto_APP.ViewModels.Carreras;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;
using System.Linq;

namespace FicProyecto_APP.ViewModels.Especialidades
{
    public class FicVmCarrerasEspecialidades : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvCatEspecialidades FicLoSrvApp;//INTERFAZ CRUD
        private string _FicLabelIdInv;

        public FicVmCarrerasEspecialidades(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatEspecialidades FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;

            FicLoSrvApp = FicPaSrvApp;
        }

        public ObservableCollection<eva_carreras_especialidades> _SfDataGrid_ItemSource_eva_carreras_especialidades;
        public ObservableCollection<eva_carreras_especialidades> SfDataGrid_ItemSource_eva_carreras_especialidades
        {
            get { return _SfDataGrid_ItemSource_eva_carreras_especialidades; }
            set
            {
                _SfDataGrid_ItemSource_eva_carreras_especialidades = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<eva_carreras_especialidades> _SfDataGrid_ItemSource_eva_carreras_especialidadesBusqueda;
        public ObservableCollection<eva_carreras_especialidades> SfDataGrid_ItemSource_eva_carreras_especialidadesBusqueda
        {
            get { return _SfDataGrid_ItemSource_eva_carreras_especialidadesBusqueda; }
            set
            {
                _SfDataGrid_ItemSource_eva_carreras_especialidadesBusqueda = value;
                RaisePropertyChanged();
            }
        }

        private eva_carreras_especialidades _SfDataGrid_SelectItem_eva_carreras_especialidades;
        public eva_carreras_especialidades SfDataGrid_SelectItem_eva_carreras_especialidades
        {
            get { return _SfDataGrid_SelectItem_eva_carreras_especialidades; }
            set
            {
                _SfDataGrid_SelectItem_eva_carreras_especialidades = value;
                RaisePropertyChanged();
            }
        }

        private ICommand AddNew;
        public ICommand FicMetAddCommand
        {
            get { return AddNew = AddNew ?? new FicVmDelegateCommand(AddNewExecute); }
        }
        private void AddNewExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCarrerasEspecialidadesInsertar>(null);
        }

        private ICommand FicMetBack;
        public ICommand FicMetBackCommand
        {
            get { return FicMetBack = FicMetBack ?? new FicVmDelegateCommand(BACKExecute); }
        }
        private void BACKExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatCarreras>(null);
        }

        private ICommand DetailsCommand;
        public ICommand FicMetDetailsCommand
        {
            get { return DetailsCommand = DetailsCommand ?? new FicVmDelegateCommand(DetailsExecute); }
        }
        private void DetailsExecute()
        {
            var temp = SfDataGrid_SelectItem_eva_carreras_especialidades;
            if (temp != null)
            {
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCarrerasEspecialidadesDetalle>(temp);
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Alerta", "Por favor seleccione una Especialidad. ", "OK");
            }
        }

        public string FicLabelIdInv
        {
            get { return _FicLabelIdInv; }
            set
            {
                if (value != null) _FicLabelIdInv = value;
                if (value == string.Empty)
                {
                    SfDataGrid_ItemSource_eva_carreras_especialidades.Clear();
                    foreach (var item in SfDataGrid_ItemSource_eva_carreras_especialidadesBusqueda)
                    {
                        SfDataGrid_ItemSource_eva_carreras_especialidades.Add(item);
                    }
                }
            }
        }

        private ICommand SearchCommand;
        public ICommand SearchCommandC
        {
            get { return SearchCommand = SearchCommand ?? new FicVmDelegateCommand(SearchCommandExecute); }
        }
        private void SearchCommandExecute()
        {
            if (FicLabelIdInv != "")
            {
                var tempRecords = SfDataGrid_ItemSource_eva_carreras_especialidadesBusqueda.Where(
                    c => c.eva_cat_carreras.DesCarrera.Contains(FicLabelIdInv) ||
                    c.eva_cat_carreras.ClaveCarrera.Contains(FicLabelIdInv) ||
                     c.eva_cat_especialidades.DesEspecialidad.Contains(FicLabelIdInv) 
                    );

                SfDataGrid_ItemSource_eva_carreras_especialidades.Clear();
                foreach (var item in tempRecords)
                {
                    SfDataGrid_ItemSource_eva_carreras_especialidades.Add(item);
                }
            }
            else
            {
                SfDataGrid_ItemSource_eva_carreras_especialidades.Clear();
                foreach (var item in SfDataGrid_ItemSource_eva_carreras_especialidadesBusqueda)
                {
                    SfDataGrid_ItemSource_eva_carreras_especialidades.Add(item);
                }
            }
        }

        public  override async void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            var rw = await FicLoSrvApp.FicGetCarrerasEspecialidades();
            SfDataGrid_ItemSource_eva_carreras_especialidades = new ObservableCollection<eva_carreras_especialidades>();
            foreach (var c in rw)
            {
                SfDataGrid_ItemSource_eva_carreras_especialidades.Add(c);
            }
            SfDataGrid_ItemSource_eva_carreras_especialidadesBusqueda = new ObservableCollection<eva_carreras_especialidades>();
            foreach (var c in SfDataGrid_ItemSource_eva_carreras_especialidades)
            {
                SfDataGrid_ItemSource_eva_carreras_especialidadesBusqueda.Add(c);
            }
        }

        private ICommand DeleteCommand;
        public ICommand FicMetDeleteCommand
        {
            get { return DeleteCommand = DeleteCommand ?? new FicVmDelegateCommand(DeleteEdificioExecute); }
        }
        private async void DeleteEdificioExecute()
        {
            var temp = SfDataGrid_SelectItem_eva_carreras_especialidades;
            if (temp != null)
            {
                var accion = await Application.Current.MainPage.DisplayActionSheet("Eliminar   " + temp.eva_cat_carreras.DesCarrera, "Cancel", null, "Si", "No");
                if (accion.Equals("Si"))
                {
                    eva_carreras_especialidades t = new eva_carreras_especialidades();
                    t.IdCarreraEspecilidad = temp.IdCarreraEspecilidad;
                    await FicLoSrvApp.FicMetRemoveCarrera_Especialidades(t);
                    FicLoSrvNavigation.FicMetNavigateTo<FicVmCarrerasEspecialidades>(null);
                }
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Alerta", "Por favor seleccione un item para poder eliminarlo. ", "OK");
            }
        }

        private ICommand EditCommand;
        public ICommand FicMetEditCommand
        {
            get { return EditCommand = EditCommand ?? new FicVmDelegateCommand(EditExecute); }
        }
        private void EditExecute()
        {
            var temp = SfDataGrid_SelectItem_eva_carreras_especialidades;
            if (temp != null)
            {
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCarrerasEspecialidadesUpdate>(temp);
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Alerta", "Por favor seleccione un item para poder modificarlo. ", "OK");
            }
        }
    }
}
