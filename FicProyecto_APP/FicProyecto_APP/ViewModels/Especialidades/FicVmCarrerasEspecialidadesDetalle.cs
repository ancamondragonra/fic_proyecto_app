﻿
using FicProyecto_APP.Interfaces.Especialidades;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using static FicProyecto_APP.Models.FicCarrera;

namespace FicProyecto_APP.ViewModels.Especialidades
{ 
    public class FicVmCarrerasEspecialidadesDetalle : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvCatEspecialidades FicLoSrvApp;//INTERFAZ CRUD


        public FicVmCarrerasEspecialidadesDetalle(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatEspecialidades FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }

        private eva_carreras_especialidades _Data;
        public eva_carreras_especialidades Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                RaisePropertyChanged();
            }
        }


        private ICommand BackNavigation;
        public ICommand BackNavgCommand
        {
            get { return BackNavigation = BackNavigation ?? new FicVmDelegateCommand(BackNavgExecute); }
        }
        public void BackNavgExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCarrerasEspecialidades>(null);
        }

        private ICommand EditCommand;
        public ICommand FicMetEditCommand
        {
            get { return EditCommand = EditCommand ?? new FicVmDelegateCommand(EditCarreraExecute); }
        }
        private void EditCarreraExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCarrerasEspecialidadesUpdate>(Data);
        }
        public override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            Data = navigationContext as eva_carreras_especialidades;
        }
    }
}
