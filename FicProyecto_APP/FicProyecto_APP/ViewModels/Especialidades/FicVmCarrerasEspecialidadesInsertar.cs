﻿using FicProyecto_APP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.Interfaces.Especialidades;
using static FicProyecto_APP.Models.FicCarrera;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace FicProyecto_APP.ViewModels.Especialidades
{
    public class FicVmCarrerasEspecialidadesInsertar : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvCatEspecialidades FicLoSrvApp;//INTERFAZ CRUD

        public FicVmCarrerasEspecialidadesInsertar(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatEspecialidades FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }

        private eva_carreras_especialidades _Data;
        public eva_carreras_especialidades Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<eva_cat_carreras> _Data_Carreras;
        public ObservableCollection<eva_cat_carreras> Data_Carreras
        {
            get { return _Data_Carreras; }
            set
            {
                _Data_Carreras = value;
                RaisePropertyChanged();
            }
        }
        public ObservableCollection<eva_cat_especialidades> _Data_Especialidades;
        public ObservableCollection<eva_cat_especialidades> Data_Especialidades
        {
            get { return _Data_Especialidades; }
            set
            {
                _Data_Especialidades = value;
                RaisePropertyChanged();
            }
        }


        private eva_cat_carreras _SelectedCarrera;
        public eva_cat_carreras SelectedCarrera
        {
            get { return _SelectedCarrera; }
            set
            {
                _SelectedCarrera = value;
                RaisePropertyChanged();
            }
        }

        private eva_cat_especialidades _SelectedEspecialidad;
        public eva_cat_especialidades SelectedEspecialidad
        {
            get { return _SelectedEspecialidad; }
            set
            {
                _SelectedEspecialidad = value;
                RaisePropertyChanged();
            }
        }



        public async override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            Data = new eva_carreras_especialidades();

            Data.Activo = "False";
            Data.Borrado = "False";
            Data.FechaIni = DateTime.Now;
            Data.FechaFin = DateTime.Now;

            var carreras = await FicLoSrvApp.FicMetGetListCarreras();
            var especialidades = await FicLoSrvApp.FicMetGetListEspecialidades();
            Data_Carreras = new ObservableCollection<eva_cat_carreras>();
            foreach (var temp in carreras)
            {
                Data_Carreras.Add(temp);
            }
            Data_Especialidades = new ObservableCollection<eva_cat_especialidades>();
            foreach (var temp in especialidades)
            {
                Data_Especialidades.Add(temp);
            }
        }

        private ICommand BackNavigation;
        public ICommand BackNavgCommand
        {
            get { return BackNavigation = BackNavigation ?? new FicVmDelegateCommand(BackNavgExecute); }
        }
        private void BackNavgExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCarrerasEspecialidades>(null);
        }

        private ICommand AddCarrera;
        public ICommand FicMetAddCommand
        {
            get { return AddCarrera = AddCarrera ?? new FicVmDelegateCommand(AddExecute); }
        }

        private void AddExecute()
        {
            if (
                (SelectedEspecialidad == null) || (SelectedCarrera == null)
                  || (DateTime.Compare(Data.FechaIni.Value.Date, Data.FechaFin.Value.Date) == 1)
                )
            {
                string mensaje = " \n";
                if (DateTime.Compare(Data.FechaIni.Value.Date, Data.FechaFin.Value.Date) == 1)
                {
                    mensaje += "Fecha final debes ser mayor que la inicial \n";
                }
                if (SelectedCarrera == null)
                {
                    mensaje += "No se a selecionado una carrera \n";
                }
                if (SelectedEspecialidad == null)
                {
                    
                    mensaje += "No se a seleccionado una especialidad \n";
                }
                Application.Current.MainPage.DisplayAlert("Alerta", "Campos vacios en " + mensaje + "", "OK");
            }
            else
            {
                if (Data.Activo.Equals("True"))
                {
                    Data.Activo = "S";
                }
                else
                {
                    Data.Activo = "N";
                }
                if (Data.Borrado.Equals("True"))
                {
                    Data.Borrado = "S";
                }
                else
                {
                    Data.Borrado = "N";
                }
                Data.UsuarioReg = "FicIbarra";
                Data.UsuarioMod = "FicIbarra";
                Data.FechaReg =DateTime.Now;
                Data.FechaUltMod = DateTime.Now;
                Data.IdCarrera = SelectedCarrera.IdCarrera;
                Data.IdEspecialidad = SelectedEspecialidad.IdEspecialidad;
                var maxid = FicLoSrvApp.FicMetGetMaxCarrerasEspecialiddadIdAsync();
                try
                {
                    if (maxid.Result == null)
                    {
                        Data.IdCarreraEspecilidad = 1;
                    }
                    else
                    {
                        Data.IdCarreraEspecilidad = (Int16)(maxid.Result.IdCarreraEspecilidad + 1);
                    }
                }
                catch (System.AggregateException e)
                {
                    Data.IdCarreraEspecilidad = 1;
                }
                FicLoSrvApp.FicMetInsertCarreraEspecialidad(Data);
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCarrerasEspecialidades>(null);
            }
        }
    }
}
