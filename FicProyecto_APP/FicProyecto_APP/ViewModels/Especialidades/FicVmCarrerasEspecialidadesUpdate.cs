﻿using FicProyecto_APP.Interfaces.Especialidades;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;

namespace FicProyecto_APP.ViewModels.Especialidades
{
    public class FicVmCarrerasEspecialidadesUpdate : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvCatEspecialidades FicLoSrvApp;//INTERFAZ CRUD

        public FicVmCarrerasEspecialidadesUpdate(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatEspecialidades FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;

            FicLoSrvApp = FicPaSrvApp;
        }
        private ICommand BackNavigation;
        public ICommand BackNavgCommand
        {
            get { return BackNavigation = BackNavigation ?? new FicVmDelegateCommand(BackNavgExecute); }
        }
        private void BackNavgExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCarrerasEspecialidades>(null);
        }

        private eva_carreras_especialidades _Data;
        public eva_carreras_especialidades Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<eva_cat_carreras> _Data_Carreras;
        public ObservableCollection<eva_cat_carreras> Data_Carreras
        {
            get { return _Data_Carreras; }
            set
            {
                _Data_Carreras = value;
                RaisePropertyChanged();
            }
        }
        public ObservableCollection<eva_cat_especialidades> _Data_Especialidades;
        public ObservableCollection<eva_cat_especialidades> Data_Especialidades
        {
            get { return _Data_Especialidades; }
            set
            {
                _Data_Especialidades = value;
                RaisePropertyChanged();
            }
        }


        private eva_cat_carreras _SelectedCarrera;
        public eva_cat_carreras SelectedCarrera
        {
            get { return _SelectedCarrera; }
            set
            {
                _SelectedCarrera = value;
                RaisePropertyChanged();
            }
        }

        private eva_cat_especialidades _SelectedEspecialidad;
        public eva_cat_especialidades SelectedEspecialidad
        {
            get { return _SelectedEspecialidad; }
            set
            {
                _SelectedEspecialidad = value;
                RaisePropertyChanged();
            }
        }

        public async override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);

            Data = (navigationContext as eva_carreras_especialidades);
            if (Data.Activo.Equals("S"))
            {
                Data.Activo = "True";
            }
            else
            {
                Data.Activo = "False";
            }
            if (Data.Borrado.Equals("S"))
            {
                Data.Borrado = "True";
            }
            else
            {
                Data.Borrado = "False";
            }

            var carreras = await FicLoSrvApp.FicMetGetListCarreras();
            var especialidades = await FicLoSrvApp.FicMetGetListEspecialidades();
            Data_Carreras = new ObservableCollection<eva_cat_carreras>();
            foreach (var temp in carreras)
            {
                if (temp.IdCarrera == Data.IdCarrera)
                {
                    SelectedCarrera = temp;
                }
                Data_Carreras.Add(temp);
            }
            Data_Especialidades = new ObservableCollection<eva_cat_especialidades>();
            foreach (var temp in especialidades)
            {
                if (temp.IdEspecialidad == Data.IdEspecialidad)
                {
                    SelectedEspecialidad = temp;
                }
                Data_Especialidades.Add(temp);
            }
        }

        private ICommand UpdateCarrera;
        public ICommand FicMetUpdateCommand
        {
            get { return UpdateCarrera = UpdateCarrera ?? new FicVmDelegateCommand(UpdateExecute); }
        }
        private void UpdateExecute()
        {
            
            if (
                (SelectedEspecialidad == null) || (SelectedCarrera == null)
                  || (DateTime.Compare(Data.FechaIni.Value.Date, Data.FechaFin.Value.Date) == 1)
                )
            {
                string mensaje = " \n";
                if (DateTime.Compare(Data.FechaIni.Value.Date, Data.FechaFin.Value.Date) == 1)
                {
                    mensaje += "Fecha final debes ser mayor que la inicial \n";
                }
                if (SelectedCarrera == null)
                {
                    mensaje += "No se a selecionado una carrera \n";
                }
                if (SelectedEspecialidad == null)
                {

                    mensaje += "No se a seleccionado una especialidad \n";
                }
                Application.Current.MainPage.DisplayAlert("Alerta", "Campos vacios en " + mensaje + "", "OK");
            }
            else
            {
                if (Data.Activo.Equals("True"))
                {
                    Data.Activo = "S";
                }
                else
                {
                    Data.Activo = "N";
                }
                if (Data.Borrado.Equals("True"))
                {
                    Data.Borrado = "S";
                }
                else
                {
                    Data.Borrado = "N";
                }
                Data.UsuarioMod = "FicIbarra";
                Data.FechaUltMod = DateTime.Now;
                Data.IdCarrera = SelectedCarrera.IdCarrera;
                Data.IdEspecialidad = SelectedEspecialidad.IdEspecialidad;
                Data.eva_cat_carreras = null;
                Data.eva_cat_especialidades = null;
                FicLoSrvApp.FicMetUpdateCarreraEspecialidad(Data);
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCarrerasEspecialidades>(null);
            }
        }
    }
}
