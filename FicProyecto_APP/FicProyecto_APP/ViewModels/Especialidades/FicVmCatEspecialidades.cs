﻿using FicProyecto_APP.Interfaces.Especialidades;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.Services.Especialidade;
using FicProyecto_APP.ViewModels.Base;
using FicProyecto_APP.ViewModels.Carreras;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;
using System.Linq;

namespace FicProyecto_APP.ViewModels.Especialidades
{
    public class FicVmCatEspecialidades : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvCatEspecialidades FicLoSrvApp;//INTERFAZ CRUD
        private string _FicLabelIdInv;

        public FicVmCatEspecialidades(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatEspecialidades FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
           
            FicLoSrvApp = FicPaSrvApp;
        }

  
        public ObservableCollection<eva_cat_especialidades> _SfDataGrid_ItemSource_eva_cat_especialidades;
        public ObservableCollection<eva_cat_especialidades> SfDataGrid_ItemSource_eva_cat_especialidades
        {
            get { return _SfDataGrid_ItemSource_eva_cat_especialidades; }
            set
            {
                _SfDataGrid_ItemSource_eva_cat_especialidades = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<eva_cat_especialidades> _SfDataGrid_ItemSource_eva_cat_especialidadesBusqueda;
        public ObservableCollection<eva_cat_especialidades> SfDataGrid_ItemSource_eva_cat_especialidadesBusqueda
        {
            get { return _SfDataGrid_ItemSource_eva_cat_especialidadesBusqueda; }
            set
            {
                _SfDataGrid_ItemSource_eva_cat_especialidadesBusqueda = value;
                RaisePropertyChanged();
            }
        }

        private eva_cat_especialidades _SfDataGrid_SelectItem_eva_cat_especialidades;
        public eva_cat_especialidades SfDataGrid_SelectItem_eva_cat_especialidades
        {
            get { return _SfDataGrid_SelectItem_eva_cat_especialidades; }
            set
            {
                _SfDataGrid_SelectItem_eva_cat_especialidades = value;
                RaisePropertyChanged();
            }
        }
       
        public async override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            var rw = await FicLoSrvApp.FicMetGetListEspecialidades();
            SfDataGrid_ItemSource_eva_cat_especialidades = new ObservableCollection<eva_cat_especialidades>();
            foreach (var c in rw)
            {
                SfDataGrid_ItemSource_eva_cat_especialidades.Add(c);
            }
            SfDataGrid_ItemSource_eva_cat_especialidadesBusqueda = new ObservableCollection<eva_cat_especialidades>();
            foreach (var c in _SfDataGrid_ItemSource_eva_cat_especialidades)
            {
                SfDataGrid_ItemSource_eva_cat_especialidadesBusqueda.Add(c);
            }
        }

        private ICommand FicMetBack;
        public ICommand FicMetBackCommand
        {
            get { return FicMetBack = FicMetBack ?? new FicVmDelegateCommand(BACKExecute); }
        }
        private void BACKExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatCarreras>(null);
        }

        private ICommand DeleteCommand;
        public ICommand FicMetDeleteCommand
        {
            get { return DeleteCommand = DeleteCommand ?? new FicVmDelegateCommand(DeleteEdificioExecute); }
        }
        private async void DeleteEdificioExecute()
        {
            var temp = SfDataGrid_SelectItem_eva_cat_especialidades;
            if (temp != null)
            {
                var accion = await Application.Current.MainPage.DisplayActionSheet("Eliminar la especialidad  " + temp.DesEspecialidad, "Cancel", null, "Si", "No");
                if (accion.Equals("Si"))
                {

                    await FicLoSrvApp.FicMetRemoveCat_Especialidades(temp);
                    try
                    {
                       
                    }
                    catch (Exception e2)
                    {
                        await Application.Current.MainPage.DisplayAlert("Alerta", "Error de relacion ", "OK");
                     
                    }
                    FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEspecialidades>(null);
                }
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Alerta", "Por favor seleccione un Edificio para poder eliminarlo. ", "OK");
            }
        }

        public string FicLabelIdInv
        {
            get { return _FicLabelIdInv; }
            set
            {
                if (value != null) _FicLabelIdInv = value;
                if (value == string.Empty)
                {
                    SfDataGrid_ItemSource_eva_cat_especialidades.Clear();
                    foreach (var item in SfDataGrid_ItemSource_eva_cat_especialidadesBusqueda)
                    {
                        SfDataGrid_ItemSource_eva_cat_especialidades.Add(item);
                    }
                }
            }
        }


        private ICommand SearchCommand;
        public ICommand SearchCommandC
        {
            get { return SearchCommand = SearchCommand ?? new FicVmDelegateCommand(SearchCommandExecute); }
        }
        private void SearchCommandExecute()
        {
            if (FicLabelIdInv != "")
            {
                var tempRecords = SfDataGrid_ItemSource_eva_cat_especialidadesBusqueda.Where(
                    c => c.DesEspecialidad.Contains(FicLabelIdInv) ||
                    c.UsuarioReg.Contains(FicLabelIdInv) 
                    );

                SfDataGrid_ItemSource_eva_cat_especialidades.Clear();
                foreach (var item in tempRecords)
                {
                    SfDataGrid_ItemSource_eva_cat_especialidades.Add(item);
                }
            }
            else
            {
                SfDataGrid_ItemSource_eva_cat_especialidades.Clear();
                foreach (var item in SfDataGrid_ItemSource_eva_cat_especialidadesBusqueda)
                {
                    SfDataGrid_ItemSource_eva_cat_especialidades.Add(item);
                }
            }
        }
        private ICommand AddNewCommand;
        public ICommand FicMetAddCommand
        {
            get { return AddNewCommand = AddNewCommand ?? new FicVmDelegateCommand(AddNewExecute); }
        }
        private void AddNewExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEspecialidadesInsertar>(null);
        }


        private ICommand DetailsCommand;
        public ICommand FicMetDetailsCommand
        {
            get { return DetailsCommand = DetailsCommand ?? new FicVmDelegateCommand(DetailsExecute); }
        }
        private void DetailsExecute()
        {
            var temp = _SfDataGrid_SelectItem_eva_cat_especialidades;
            if (temp != null)
            {
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEspecialidadesDetalle>(temp);
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Alerta", "Por favor seleccione una Especialidad. ", "OK");
            }

        }

        private ICommand EditCommand;
        public ICommand FicMetEditCommand
        {
            get { return EditCommand = EditCommand ?? new FicVmDelegateCommand(EditExecute); }
        }
        private void EditExecute()
        {
            var temp = _SfDataGrid_SelectItem_eva_cat_especialidades;
            if (temp != null)
            {
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEspecialidadesUpdate>(temp);
            }
            else
            {
                Application.Current.MainPage.DisplayAlert("Alerta", "Por favor seleccione un item para poder modificarlo. ", "OK");
            }
        }
    }
}
