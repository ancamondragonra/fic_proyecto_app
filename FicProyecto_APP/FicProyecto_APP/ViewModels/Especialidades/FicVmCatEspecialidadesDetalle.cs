﻿using FicProyecto_APP.Interfaces.Especialidades;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using static FicProyecto_APP.Models.FicCarrera;

namespace FicProyecto_APP.ViewModels.Especialidades
{
    public class FicVmCatEspecialidadesDetalle : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvCatEspecialidades FicLoSrvApp;//INTERFAZ CRUD

        private eva_cat_especialidades _Data;
        public eva_cat_especialidades Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                RaisePropertyChanged();
            }
        }


        public FicVmCatEspecialidadesDetalle(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatEspecialidades FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }
 

        private ICommand BackNavigation;
        public ICommand BackNavgCommand
        {
            get { return BackNavigation = BackNavigation ?? new FicVmDelegateCommand(BackNavgExecute); }
        }
        public void BackNavgExecute()
        {
            //FicLoSrvNavigation.FicMetNavigateBack();
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEspecialidades>(null);
        }

        private ICommand EditCommand;
        public ICommand FicMetEditCommand
        {
            get { return EditCommand = EditCommand ?? new FicVmDelegateCommand(EditCarreraExecute); }
        }
        private void EditCarreraExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEspecialidadesUpdate>(Data);
        }
        public override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            Data = navigationContext as eva_cat_especialidades;
        }
    }
}
