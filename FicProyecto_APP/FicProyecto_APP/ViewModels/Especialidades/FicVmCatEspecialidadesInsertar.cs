﻿using FicProyecto_APP.Interfaces.Especialidades;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;

namespace FicProyecto_APP.ViewModels.Especialidades
{
    public class FicVmCatEspecialidadesInsertar : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvCatEspecialidades FicLoSrvApp;//INTERFAZ CRUD

        public FicVmCatEspecialidadesInsertar(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatEspecialidades FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }
        private eva_cat_especialidades _Data;
        public eva_cat_especialidades Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                RaisePropertyChanged();
            }
        }
        private ICommand BackNavigation;
        public ICommand BackNavgCommand
        {
            get { return BackNavigation = BackNavigation ?? new FicVmDelegateCommand(BackNavgExecute); }
        }
        private void BackNavgExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEspecialidades>(null);
        }

        public async override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            Data = new eva_cat_especialidades();
            Data.Activo = "False";
            Data.Borrado = "False";

        }
        private ICommand AddCommand;
        public ICommand FicMetAddCommand
        {
            get { return AddCommand = AddCommand ?? new FicVmDelegateCommand(AddExecute); }
        }

        private void AddExecute()
        {
            if (
                (Data.DesEspecialidad == null)
                )
            {
                string mensaje = " \n";
                if (Data.DesEspecialidad == null)
                {
                    mensaje += "DesEspecialidad \n";
                }

                Application.Current.MainPage.DisplayAlert("Alerta", "Campos vacios en " + mensaje + "", "OK");
            }
            else
            {
                if (Data.Activo.Equals("True"))
                {
                    Data.Activo = "S";
                }
                else
                {
                    Data.Activo = "N";
                }
                if (Data.Borrado.Equals("True"))
                {
                    Data.Borrado = "S";
                }
                else
                {
                    Data.Borrado = "N";
                }
                Data.UsuarioReg = "FicIbarra";
                Data.UsuarioMod = "FicIbarra";
                Data.FechaReg = DateTime.Now;
                Data.FechaUltMod = DateTime.Now;

                var maxid = FicLoSrvApp.FicMetGetMaxCatEspecialiddadIdAsync();
                try
                {
                    if (maxid.Result == null)
                    {
                        Data.IdEspecialidad = 1;
                    }
                    else
                    {
                        Data.IdEspecialidad = (Int16)(maxid.Result.IdEspecialidad + 1);
                    }
                }
                catch (System.AggregateException e)
                {
                    Data.IdEspecialidad = 1;
                }
                FicLoSrvApp.FicMetInsertCatEspecialidad(Data);
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEspecialidades>(null);
            }
        }
    }
}
