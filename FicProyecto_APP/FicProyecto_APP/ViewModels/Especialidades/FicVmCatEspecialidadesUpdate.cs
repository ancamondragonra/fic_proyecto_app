﻿using FicProyecto_APP.Interfaces.Especialidades;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using static FicProyecto_APP.Models.FicCarrera;

namespace FicProyecto_APP.ViewModels.Especialidades
{
    public class FicVmCatEspecialidadesUpdate : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvCatEspecialidades FicLoSrvApp;//INTERFAZ CRUD

        public FicVmCatEspecialidadesUpdate(IFicSrvNavigation FicPaSrvNavigation, IFicSrvCatEspecialidades FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }
        private ICommand BackNavigation;
        public ICommand BackNavgCommand
        {
            get { return BackNavigation = BackNavigation ?? new FicVmDelegateCommand(BackNavgExecute); }
        }
        private void BackNavgExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEspecialidades>(null);
        }

        private eva_cat_especialidades _Data;
        public eva_cat_especialidades Data
        {
            get { return _Data; }
            set
            {
                _Data = value;
                RaisePropertyChanged();
            }
        }

        public async override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);

            Data = (navigationContext as eva_cat_especialidades);
            if (Data.Activo.Equals("S"))
            {
                Data.Activo = "True";
            }
            else
            {
                Data.Activo = "False";
            }
            if (Data.Borrado.Equals("S"))
            {
                Data.Borrado = "True";
            }
            else
            {
                Data.Borrado = "False";
            }
        }

        private ICommand UpdateCarrera;
        public ICommand FicMetUpdateCommand
        {
            get { return UpdateCarrera = UpdateCarrera ?? new FicVmDelegateCommand(UpdateExecute); }
        }
        private void UpdateExecute()
        {
            if (Data.DesEspecialidad == null)
            {
                string mensaje = " \n";
                if (Data.DesEspecialidad == null)
                {
                    mensaje += "Fecha erronea \n";
                }
                Application.Current.MainPage.DisplayAlert("Alerta", "Campos vacios en " + mensaje + "", "OK");
            }
            else
            {
                if (Data.Activo.Equals("True"))
                {
                    Data.Activo = "S";
                }
                else
                {
                    Data.Activo = "N";
                }
                if (Data.Borrado.Equals("True"))
                {
                    Data.Borrado = "S";
                }
                else
                {
                    Data.Borrado = "N";
                }
                Data.UsuarioMod = "FicIbarra";
                Data.FechaUltMod = DateTime.Now;

                FicLoSrvApp.FicMetUpdateCatEspecialidad(Data);
                FicLoSrvNavigation.FicMetNavigateTo<FicVmCatEspecialidades>(null);
            }
        }
    }
}
