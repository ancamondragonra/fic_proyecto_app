﻿using FicProyecto_APP.Interfaces.Generales;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using static FicProyecto_APP.Models.FicCarrera;
using System.Linq;

namespace FicProyecto_APP.ViewModels.Generales
{
    public class FicVmGenerales : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvGenerales FicLoSrvApp;//INTERFAZ CRUD
        private string _FicLabelIdInv;

        public FicVmGenerales(IFicSrvNavigation FicPaSrvNavigation, IFicSrvGenerales FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }

        public ObservableCollection<cat_generales> _SfDataGrid_ItemSource_Data;
        public ObservableCollection<cat_generales> SfDataGrid_ItemSource_Data
        {
            get { return _SfDataGrid_ItemSource_Data; }
            set
            {
                _SfDataGrid_ItemSource_Data = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<cat_generales> _SfDataGrid_ItemSource_DataB;
        public ObservableCollection<cat_generales> SfDataGrid_ItemSource_DataB
        {
            get { return _SfDataGrid_ItemSource_DataB; }
            set
            {
                _SfDataGrid_ItemSource_DataB = value;
                RaisePropertyChanged();
            }
        }


        private cat_generales _SfDataGrid_SelectItem_Data;
        public cat_generales SfDataGrid_SelectItem_Data
        {
            get { return _SfDataGrid_SelectItem_Data; }
            set
            {
                _SfDataGrid_SelectItem_Data = value;
                RaisePropertyChanged();
            }
        }



        public string FicLabelIdInv
        {
            get { return _FicLabelIdInv; }
            set
            {
                if (value != null) _FicLabelIdInv = value;
                if (value == string.Empty)
                {
                    SfDataGrid_ItemSource_Data.Clear();
                    foreach (var item in SfDataGrid_ItemSource_DataB)
                    {
                        SfDataGrid_ItemSource_Data.Add(item);
                    }
                }
            }
        }


        private ICommand SearchCommand;
        public ICommand SearchCommandC
        {
            get { return SearchCommand = SearchCommand ?? new FicVmDelegateCommand(SearchCommandExecute); }
        }
        private void SearchCommandExecute()
        {
            if (FicLabelIdInv != "")
            {
                var tempRecords = SfDataGrid_ItemSource_DataB.Where(
                    c => c.Clave.Contains(FicLabelIdInv) ||
                    c.DesGeneral.Contains(FicLabelIdInv) 
                    );

                SfDataGrid_ItemSource_Data.Clear();
                foreach (var item in tempRecords)
                {
                    SfDataGrid_ItemSource_Data.Add(item);
                }
            }
            else
            {
                SfDataGrid_ItemSource_Data.Clear();
                foreach (var item in SfDataGrid_ItemSource_DataB)
                {
                    SfDataGrid_ItemSource_Data.Add(item);
                }
            }
        }
        private ICommand OpenView;
        public ICommand ShowGeneralesTipo
        {
            get { return OpenView = OpenView ?? new FicVmDelegateCommand(ShowEspecialidadesEX); }
        }
        private void ShowEspecialidadesEX()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmGeneralesTipo>(null);
        }
        public async override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            var res = await FicLoSrvApp.FicGetGenerales();
            SfDataGrid_ItemSource_Data = new ObservableCollection<cat_generales>();
            foreach (var temp in res)
            {
                SfDataGrid_ItemSource_Data.Add(temp);
            }
            SfDataGrid_ItemSource_DataB = new ObservableCollection<cat_generales>();
            foreach (var item in SfDataGrid_ItemSource_Data)
            {
                SfDataGrid_ItemSource_DataB.Add(item);
            }
        }
    }
}
