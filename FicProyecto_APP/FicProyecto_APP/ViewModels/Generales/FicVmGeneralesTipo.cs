﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using FicProyecto_APP.ViewModels.Base;
using FicProyecto_APP.Navegacion;
using FicProyecto_APP.Interfaces.Generales;
using static FicProyecto_APP.Models.FicCarrera;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace FicProyecto_APP.ViewModels.Generales
{
    public class FicVmGeneralesTipo : FicViewModelBase
    {
        private IFicSrvNavigation FicLoSrvNavigation;//INTERFAZ NVEGACION
        private IFicSrvGenerales FicLoSrvApp;//INTERFAZ CRUD
        private string _FicLabelIdInv;

        public FicVmGeneralesTipo(IFicSrvNavigation FicPaSrvNavigation, IFicSrvGenerales FicPaSrvApp)
        {
            FicLoSrvNavigation = FicPaSrvNavigation;
            FicLoSrvApp = FicPaSrvApp;
        }
        private ICommand FicMetBack;
        public ICommand FicMetBackCommand
        {
            get { return FicMetBack = FicMetBack ?? new FicVmDelegateCommand(BACKExecute); }
        }
        private void BACKExecute()
        {
            FicLoSrvNavigation.FicMetNavigateTo<FicVmGenerales>(null);
        }

        public ObservableCollection<cat_tipos_generales> _SfDataGrid_ItemSource_Data;
        public ObservableCollection<cat_tipos_generales> SfDataGrid_ItemSource_Data
        {
            get { return _SfDataGrid_ItemSource_Data; }
            set
            {
                _SfDataGrid_ItemSource_Data = value;
                RaisePropertyChanged();
            }
        }

        public ObservableCollection<cat_tipos_generales> _SfDataGrid_ItemSource_DataB;
        public ObservableCollection<cat_tipos_generales> SfDataGrid_ItemSource_DataB
        {
            get { return _SfDataGrid_ItemSource_DataB; }
            set
            {
                _SfDataGrid_ItemSource_DataB = value;
                RaisePropertyChanged();
            }
        }


        private cat_tipos_generales _SfDataGrid_SelectItem_Data;
        public cat_tipos_generales SfDataGrid_SelectItem_Data
        {
            get { return _SfDataGrid_SelectItem_Data; }
            set
            {
                _SfDataGrid_SelectItem_Data = value;
                RaisePropertyChanged();
            }
        }



        public string FicLabelIdInv
        {
            get { return _FicLabelIdInv; }
            set
            {
                if (value != null) _FicLabelIdInv = value;
                if (value == string.Empty)
                {
                    SfDataGrid_ItemSource_Data.Clear();
                    foreach (var item in SfDataGrid_ItemSource_DataB)
                    {
                        SfDataGrid_ItemSource_Data.Add(item);
                    }
                }
            }
        }


        private ICommand SearchCommand;
        public ICommand SearchCommandC
        {
            get { return SearchCommand = SearchCommand ?? new FicVmDelegateCommand(SearchCommandExecute); }
        }
        private void SearchCommandExecute()
        {
            if (FicLabelIdInv != "")
            {
                var tempRecords = SfDataGrid_ItemSource_DataB.Where(
                    c => c.DesTipo.Contains(FicLabelIdInv) 
                    );

                SfDataGrid_ItemSource_Data.Clear();
                foreach (var item in tempRecords)
                {
                    SfDataGrid_ItemSource_Data.Add(item);
                }
            }
            else
            {
                SfDataGrid_ItemSource_Data.Clear();
                foreach (var item in SfDataGrid_ItemSource_DataB)
                {
                    SfDataGrid_ItemSource_Data.Add(item);
                }
            }
        }

        public async override void OnAppearingAsync(object navigationContext)
        {
            base.OnAppearingAsync(navigationContext);
            var res = await FicLoSrvApp.FicGetGeneralesTipo();
            SfDataGrid_ItemSource_Data = new ObservableCollection<cat_tipos_generales>();
            foreach (var temp in res)
            {
                SfDataGrid_ItemSource_Data.Add(temp);
            }
            SfDataGrid_ItemSource_DataB = new ObservableCollection<cat_tipos_generales>();
            foreach (var item in SfDataGrid_ItemSource_Data)
            {
                SfDataGrid_ItemSource_DataB.Add(item);
            }
        }
    }
}
