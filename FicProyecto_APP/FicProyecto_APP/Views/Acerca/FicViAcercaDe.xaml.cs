﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Acerca
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViAcercaDe : ContentPage
	{
        private object FicParameter { get; set; }
        public FicViAcercaDe ()
		{
			InitializeComponent ();
		}
        public FicViAcercaDe(object FicParameter)
        {
            InitializeComponent();
            BindingContext = App.FicMetLocator.FicVmCatCarreras;
            this.FicParameter = null;
            this.Title = "Acerca de";
        }

    }
}