﻿using FicProyecto_APP.ViewModels.Carreras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Carreras
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViCarrerasAdd : ContentPage
	{
        private object FicLoParameter;
        public DatePicker DatePickerfecha1, DatePickerfecha2;
        public Picker pickerGrado;
        public FicViCarrerasAdd (object FicParameter)
		{
			InitializeComponent ();
            FicLoParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmCatCarrerasAdd;

            
            DatePickerfecha1 = this.FindByName<DatePicker>("FechaIni");
            DatePickerfecha2 = this.FindByName<DatePicker>("FechaFin");

            DatePickerfecha2.Date = DateTime.Now;
            DatePickerfecha1.Date = DateTime.Now;
            
            
            pickerGrado = this.FindByName<Picker>("IdGenGradoEscolar");
            pickerGrado.SelectedIndex = -1;
        }

        public void Handle_ValueChanged(object sender, Syncfusion.SfNumericUpDown.XForms.ValueEventArgs e)
        {
            (BindingContext as FicVmCatCarrerasAdd).Carreras.Creditos = Int16.Parse(e.Value.ToString());
        }


        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCatCarrerasAdd;
            if (viewModel != null)
            {
                viewModel.OnAppearingAsync(FicLoParameter);

            }
            this.Title = "Agregar Carrera";
        }
        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCatCarrerasAdd;
            if (viewModel != null) viewModel.OnDisappearing();
        }


    }
}