﻿using FicProyecto_APP.Services.Carreras;
using FicProyecto_APP.ViewModels.Carreras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static FicProyecto_APP.Models.FicCarrera;

namespace FicProyecto_APP.Views.Carreras
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViCarrerasDetail : ContentPage
	{
        private object FicParameter { get; set; }
        FicSrvCatCarrerasList service { get; set; }
        public FicViCarrerasDetail (object FicParameter)
		{
			InitializeComponent ();
            service = new FicSrvCatCarrerasList();
            this.FicParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmCatCarrerasDetail;
        }

        protected async void FicMetDeleteCommand(object sender, EventArgs e)
        {
            var context = BindingContext as FicVmCatCarrerasDetail;
            bool conf = await DisplayAlert("Cuidado", "¿Desea eliminar este elemento?", "Sí", "No");
            if (conf)
            {
                eva_cat_carreras t =new eva_cat_carreras();
                t.IdCarrera=context.Carrera.IdCarrera;
                await service.FicMetRemoveCarrera(t);
                context.BackNavgExecute();
            }
        }

        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCatCarrerasDetail;
            if (viewModel != null) viewModel.OnAppearingAsync(FicParameter);
            this.Title = "Detalle Carrera";
        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCatCarrerasDetail;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}