﻿using FicProyecto_APP.Services.Carreras;
using FicProyecto_APP.ViewModels.Carreras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Carreras
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViCarrerasList : ContentPage
	{
        private object FicParameter { get; set; }

        public FicViCarrerasList ()
		{
			InitializeComponent ();
            this.FicParameter = null;
            BindingContext = App.FicMetLocator.FicVmCatCarreras;
          
        }
        public FicViCarrerasList(object FicParameter)
        {
            InitializeComponent();
            BindingContext = App.FicMetLocator.FicVmCatCarreras;
            this.FicParameter = null;
            this.Title = "Lista de Carreras";
        }

        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCatCarreras;
            if (viewModel != null) viewModel.OnAppearingAsync(FicParameter);
        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCatCarreras;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}