﻿using FicProyecto_APP.ViewModels.Carreras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Carreras
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViCarrerasUpdate : ContentPage
    {
        private object FicLoParameter;
        public FicViCarrerasUpdate (object FicParameter)
		{
			InitializeComponent ();
            FicLoParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmCatCarrerasUpdate;
        }

        public void Handle_ValueChanged(object sender, Syncfusion.SfNumericUpDown.XForms.ValueEventArgs e)
        {
            (BindingContext as FicVmCatCarrerasUpdate).Carreras.Creditos = Int16.Parse(e.Value.ToString());
        }

        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCatCarrerasUpdate;
            if (viewModel != null)
            {
                viewModel.OnAppearingAsync(FicLoParameter);
            }
            this.Title = "Actualizar Carrera";
        }
        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCatCarrerasUpdate;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}