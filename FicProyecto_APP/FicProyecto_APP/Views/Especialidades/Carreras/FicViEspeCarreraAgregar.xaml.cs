﻿using FicProyecto_APP.ViewModels.Especialidades.Carreras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Especialidades.Carreras
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViEspeCarreraAgregar : ContentPage
	{
        private object FicParameter { get; set; }
        public FicViEspeCarreraAgregar (object FicParameter)
		{
			InitializeComponent ();
            this.FicParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmEspeCarreraAdd;
        }
        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmEspeCarreraAdd;
            if (viewModel != null) viewModel.OnAppearingAsync(FicParameter);
            this.Title = "Agregar  Especialidades  a  " + viewModel.temp1.DesCarrera + " " + viewModel.temp1.ClaveCarrera;
        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmEspeCarreraAdd;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}