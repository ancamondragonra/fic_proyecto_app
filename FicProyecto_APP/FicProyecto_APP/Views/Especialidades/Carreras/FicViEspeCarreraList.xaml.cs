﻿using FicProyecto_APP.ViewModels.Especialidades.Carreras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Especialidades.Carreras
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViEspeCarreraList : ContentPage
	{
        private object FicParameter { get; set; }
        public FicViEspeCarreraList (object FicParameter)
		{
			InitializeComponent ();
            this.FicParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmEspeCarrera;
        }
        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmEspeCarrera;
            if (viewModel != null) viewModel.OnAppearingAsync(FicParameter);
            this.Title = "Lista de  Especialidades  de  "+viewModel.temp.DesCarrera+" " + viewModel.temp.ClaveCarrera;
        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmEspeCarrera;
            if (viewModel != null) viewModel.OnDisappearing();
        }

    }
}