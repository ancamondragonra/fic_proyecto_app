﻿using FicProyecto_APP.Services.Especialidade;
using FicProyecto_APP.ViewModels.Especialidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static FicProyecto_APP.Models.FicCarrera;

namespace FicProyecto_APP.Views.Especialidades
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViEspecialidadesCarreraDetalle : ContentPage
	{
        private object FicParameter { get; set; }
        FicSrvCatEspecialidades service { get; set; }
        public FicViEspecialidadesCarreraDetalle ()
		{
			InitializeComponent ();
            service = new FicSrvCatEspecialidades();
            this.FicParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmCarrerasEspecialidadesDetalle;
        }
        public FicViEspecialidadesCarreraDetalle(object FicParameter)
        {
            InitializeComponent();
            service = new FicSrvCatEspecialidades();
            this.FicParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmCarrerasEspecialidadesDetalle;
        }
        protected async void FicMetDeleteCommand(object sender, EventArgs e)
        {
            var context = BindingContext as FicVmCarrerasEspecialidadesDetalle;
            bool conf = await DisplayAlert("Cuidado", "¿Desea eliminar este elemento?", "Sí", "No");
            if (conf)
            {
                eva_carreras_especialidades t = new eva_carreras_especialidades();
                t.IdCarreraEspecilidad = context.Data.IdCarreraEspecilidad;
                 await service.FicMetRemoveCarrera_Especialidades(t);
                context.BackNavgExecute();
            }
        }
        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCarrerasEspecialidadesDetalle;
            if (viewModel != null) viewModel.OnAppearingAsync(FicParameter);
            this.Title = "Detalle Carrera Especialidad";
        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCarrerasEspecialidadesDetalle;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}