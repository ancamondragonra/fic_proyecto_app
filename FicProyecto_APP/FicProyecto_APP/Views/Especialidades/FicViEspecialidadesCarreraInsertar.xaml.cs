﻿using FicProyecto_APP.ViewModels.Especialidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Especialidades
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FicViEspecialidadesCarreraInsertar : ContentPage
    {
        private object FicLoParameter;
        public DatePicker DatePickerfecha1, DatePickerfecha2;
        public FicViEspecialidadesCarreraInsertar(object FicParameter)
        {
            InitializeComponent();
            FicLoParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmCarrerasEspecialidadesInsertar;

            DatePickerfecha1 = this.FindByName<DatePicker>("FechaIni");
            DatePickerfecha2 = this.FindByName<DatePicker>("FechaFin");
            DatePickerfecha1.Date = DateTime.Now;
            DatePickerfecha2.Date = DateTime.Now;

        }
        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCarrerasEspecialidadesInsertar;
            if (viewModel != null)
            {
                viewModel.OnAppearingAsync(FicLoParameter);

            }
            this.Title = "Agregar Carrera con especialidad";
        }
        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCarrerasEspecialidadesInsertar;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}