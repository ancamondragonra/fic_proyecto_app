﻿using FicProyecto_APP.ViewModels.Especialidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Especialidades
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViEspecialidadesCarreraList : ContentPage
	{
        private object FicParameter { get; set; }
        public FicViEspecialidadesCarreraList ()
		{
			InitializeComponent ();
		}
        public FicViEspecialidadesCarreraList(object FicParameter)
        {
            InitializeComponent();
            this.FicParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmCarrerasEspecialidades;
        }
        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCarrerasEspecialidades;
            if (viewModel != null) viewModel.OnAppearingAsync(FicParameter);
            this.Title = "Lista de Carreras con Especialidades ";
        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCarrerasEspecialidades;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}