﻿using FicProyecto_APP.ViewModels.Especialidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Especialidades
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViEspecialidadesCarreraUpdate : ContentPage
	{
        private object FicLoParameter;
        public FicViEspecialidadesCarreraUpdate (object FicParameter)
		{
			InitializeComponent ();
            FicLoParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmCarrerasEspecialidadesUpdate;
        }
        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCarrerasEspecialidadesUpdate;
            if (viewModel != null)
            {
                viewModel.OnAppearingAsync(FicLoParameter);
            }
            this.Title = "Actualizar Carrera Especialidad";
        }
        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCarrerasEspecialidadesUpdate;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}