﻿
using FicProyecto_APP.Services.Especialidade;
using FicProyecto_APP.ViewModels.Especialidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static FicProyecto_APP.Models.FicCarrera;

namespace FicProyecto_APP.Views.Especialidades
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViEspecialidadesDetalle : ContentPage
	{
        private object FicParameter { get; set; }
        private FicSrvCatEspecialidades service { get; set; }
        public FicViEspecialidadesDetalle(object FicParameter)
        {
            InitializeComponent();
            service = new FicSrvCatEspecialidades();
            this.FicParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmCatEspecialidadesDetalle;
        }

        protected async void FicMetDeleteCommand(object sender, EventArgs e)
        {
            var context = BindingContext as FicVmCatEspecialidadesDetalle;
            bool conf = await DisplayAlert("Cuidado", "¿Desea eliminar este elemento?", "Sí", "No");
            if (conf)
            {
               eva_cat_especialidades t = new eva_cat_especialidades();
                t.IdEspecialidad = context.Data.IdEspecialidad;
                try{
                    await service.FicMetRemoveCat_Especialidades(t);
                    context.BackNavgExecute();
                }
                catch (Exception e1)
                {
                    await DisplayAlert("Alerta", "Existe una relacion. ", "OK");
                    context.BackNavgExecute();
                }
              
            }
        }
        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCatEspecialidadesDetalle;
            if (viewModel != null) viewModel.OnAppearingAsync(FicParameter);
            this.Title = "Detalle  Especialidades";
        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCatEspecialidadesDetalle;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}