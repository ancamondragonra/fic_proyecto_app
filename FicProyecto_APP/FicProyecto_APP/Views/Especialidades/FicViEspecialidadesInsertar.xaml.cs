﻿using FicProyecto_APP.ViewModels.Especialidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Especialidades
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViEspecialidadesInsertar : ContentPage
	{
        private object FicLoParameter;
        public FicViEspecialidadesInsertar (object FicParameter)
		{
			InitializeComponent ();
            FicLoParameter = FicParameter;
            BindingContext = App.FicMetLocator.FicVmCatEspecialidadesInsertar;
        }
        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCatEspecialidadesInsertar;
            if (viewModel != null)
            {
                viewModel.OnAppearingAsync(FicLoParameter);

            }
            this.Title = "Agregar especialidad";
        }
        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCatEspecialidadesInsertar;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}