﻿using FicProyecto_APP.Services.Especialidade;
using FicProyecto_APP.ViewModels.Especialidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Especialidades
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FicViEspecialidadesList : ContentPage
    {
     
        private object FicParameter { get; set; }
        FicSrvCatEspecialidades service { get; set; }
        
        public FicViEspecialidadesList()
        {
            InitializeComponent();
            this.FicParameter = null;
            service = new FicSrvCatEspecialidades();
            BindingContext = App.FicMetLocator.FicVmCatEspecialidades;
  
        }
        
        
        public FicViEspecialidadesList(object FicParameter)
        {
            InitializeComponent();
            this.FicParameter = FicParameter;
            service = new FicSrvCatEspecialidades();
            BindingContext = App.FicMetLocator.FicVmCatEspecialidades;
        }
        

        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmCatEspecialidades;
            if (viewModel != null) viewModel.OnAppearingAsync(FicParameter);
            this.Title = "Lista de Especialidades";
        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmCatEspecialidades;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}