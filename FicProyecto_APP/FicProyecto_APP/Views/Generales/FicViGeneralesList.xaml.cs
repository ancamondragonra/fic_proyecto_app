﻿using FicProyecto_APP.Services.Generales;
using FicProyecto_APP.ViewModels.Generales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Generales
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViGeneralesList : ContentPage
	{
        private object FicParameter { get; set; }
        FicSrvCatGenerales service { get; set; }
        public FicViGeneralesList ()
		{
			InitializeComponent ();
            BindingContext = App.FicMetLocator.FicVmGenerales;
            this.FicParameter = null;
            service = new FicSrvCatGenerales();
            this.Title = "Lista Generales";
        }
        public FicViGeneralesList(object FicParameter)
        {
            InitializeComponent();
            BindingContext = App.FicMetLocator.FicVmGenerales;
            this.FicParameter = null;
            service = new FicSrvCatGenerales();
            this.Title = "Lista Generales";
        }
        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmGenerales;
            if (viewModel != null) viewModel.OnAppearingAsync(FicParameter);
        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmGenerales;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}