﻿using FicProyecto_APP.Services.Generales;
using FicProyecto_APP.ViewModels.Generales;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Generales
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViGeneralesTipoList : ContentPage
	{
        private object FicParameter { get; set; }
        FicSrvCatGenerales service { get; set; }
        public FicViGeneralesTipoList ()
		{
			InitializeComponent ();
            BindingContext = App.FicMetLocator.FicVmGenerales;
            this.FicParameter = null;
            service = new FicSrvCatGenerales();
            this.Title = "Lista Generales Tipo";
        }

        public FicViGeneralesTipoList(object FicParameter)
        {
            InitializeComponent();
            BindingContext = App.FicMetLocator.FicVmGeneralesTipo;
            this.FicParameter = null;
            service = new FicSrvCatGenerales();
            this.Title = "Lista Generales Tipo";
        }
        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmGeneralesTipo;
            if (viewModel != null) viewModel.OnAppearingAsync(FicParameter);
        }

        protected override void OnDisappearing()
        {
            var viewModel = BindingContext as FicVmGeneralesTipo;
            if (viewModel != null) viewModel.OnDisappearing();
        }
    }
}