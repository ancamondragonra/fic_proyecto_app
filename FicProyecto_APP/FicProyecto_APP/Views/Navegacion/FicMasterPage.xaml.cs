﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FicProyecto_APP.Views.Acerca;
using FicProyecto_APP.Views.Carreras;
using FicProyecto_APP.Views.Especialidades;
using FicProyecto_APP.Views.Generales;
using FicProyecto_APP.Views.Navegacion;
using FicProyecto_APP.Views.WebApi;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Navegacion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicMasterPage : MasterDetailPage
    {
		public FicMasterPage ()
		{
            InitializeComponent();
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }
        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                var FicItemMenu = e.SelectedItem as FicMasterPageMenuItem;
                if (FicItemMenu == null)
                    return;

                var FicPagina = FicItemMenu.FicPageName as string;
                switch (FicPagina)
                {
                    case "FicViCatEdificiosList":
                          FicItemMenu.TargetType = typeof(FicViCarrerasList);
                          break;
                    case "FicViImportarWebApi":
                          FicItemMenu.TargetType = typeof(FicViImportarWebApi);
                          break;
                      case "FicViExportarWebApi":
                          FicItemMenu.TargetType = typeof(FicViExportarWebApi);
                          break;
                    case "FicViAcercaDe":
                        FicItemMenu.TargetType = typeof(FicViAcercaDe);
                        break;
                    case "FicViGeneralesList":
                        FicItemMenu.TargetType = typeof(FicViGeneralesList);
                        break;
                    default:
                          break;
                }
                
                object[] FicObjeto = new object[1];
                //FIC: Sin enviar parametro
                var FicPageOpen = (Page)Activator.CreateInstance(FicItemMenu.TargetType);
                //var FicPageOpen = Activator.CreateInstance(typeof(FicViInventarioList)) as Page;
                //FIC: Enviando como parametro un objeto vacio
                FicPageOpen.Title = FicItemMenu.Title;
                Detail = new NavigationPage(FicPageOpen);
                IsPresented = false;
                MasterPage.ListView.SelectedItem = null;
            }
            catch (Exception ex)
            {
                new Page().DisplayAlert("ERROR", ex.Message.ToString(), "OK");
            }

        }

    }
}