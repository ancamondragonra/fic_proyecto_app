﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.Navegacion
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicMasterPageMaster : ContentPage
	{
        public ListView ListView;


        public FicMasterPageMaster ()
		{
			InitializeComponent ();
            BindingContext = new FicMasterPageMasterViewModel();
            ListView = MenuItemsListView;

        }

        class FicMasterPageMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<FicMasterPageMenuItem> MenuItems { get; set; }

            public FicMasterPageMasterViewModel()
            {
                MenuItems = new ObservableCollection<FicMasterPageMenuItem>(new[]
                {
                    new FicMasterPageMenuItem { Id = 0, FicPageName = "FicViCatEdificiosList", Title = "Lista de Carreras"},
                    new FicMasterPageMenuItem { Id = 2, FicPageName = "FicViGeneralesList", Title = "Lista  Generales"},
                    new FicMasterPageMenuItem { Id = 3, FicPageName = "FicViImportarWebApi", Title = "Importar WEB API"},
                    new FicMasterPageMenuItem { Id = 4, FicPageName = "FicViExportarWebApi", Title = "Exportar WEB API"},
                    new FicMasterPageMenuItem { Id = 2, FicPageName = "FicViAcercaDe", Title = "Acerca de..."},

                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }

    }
}