﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FicProyecto_APP.Views.Navegacion
{
    class FicMasterPageMenuItem
    {
        public FicMasterPageMenuItem()
        {
            TargetType = typeof(FicMasterPageDetail);
        }
        //CLASE QUE SE USA PARA DEFINIR EL MENU LATERAL
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }

        public string Icon { get; set; }

        public string FicPageName { get; set; }

    }
}
