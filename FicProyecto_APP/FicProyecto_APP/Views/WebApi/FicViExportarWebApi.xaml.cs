﻿using FicProyecto_APP.ViewModels.ImportarExportar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.WebApi
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViExportarWebApi : ContentPage
	{
		public FicViExportarWebApi ()
		{
			InitializeComponent ();
            BindingContext = App.FicMetLocator.FicVmExportarWebApi;


        }
        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmExportarWebApi;
            if (viewModel != null)
            {
                viewModel.OnAppearing();
            }
        }

    }
}