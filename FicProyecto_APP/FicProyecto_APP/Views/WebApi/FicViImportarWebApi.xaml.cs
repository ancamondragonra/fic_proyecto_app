﻿using FicProyecto_APP.ViewModels.ImportarExportar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FicProyecto_APP.Views.WebApi
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FicViImportarWebApi : ContentPage
	{

		public FicViImportarWebApi ()
		{
			InitializeComponent ();
            BindingContext = App.FicMetLocator.FicVmImportarWebApi;
        }
        protected override void OnAppearing()
        {
            var viewModel = BindingContext as FicVmImportarWebApi;
            if (viewModel != null)
            {
                viewModel.OnAppearing();
            }
        }

    }
}